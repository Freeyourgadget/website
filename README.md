# Website

A documentation and home page used as website for [Gadgetbridge app](https://codeberg.org/Freeyourgadget/Gadgetbridge).

## Contribute

Contributions are welcome; thanks for taking time into improving the documentation!

The documentation uses [Mkdocs](https://www.mkdocs.org/) (specifically, with [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/) theme) to generate a static site from [Markdown](https://commonmark.org/) files. So if you are already familiar with Markdown, you can check out the existing pages to contribute to and send a pull request for it if you would like.

Within the website, there is a "eye" icon in the top corner of every page that directly takes you to the source Markdown file of the current page. In addition to that, there is also a "report" icon that takes you to the creating a new issue, so you can still contribute even if you are not familiar with Git.

The configuration of the documentation lies at [`mkdocs.yml`](./mkdocs.yml) file. There is also a [`extra_config.yml`](./extra_config.yml) file for storing custom configuration that is specific to this documentation. Since it is going to be merged with `mkdocs.yml` during build step, they are separated for better readability.

If you have or/and know a gadget that works with Gadgetbridge, you can add (or update, if it already exists) its entry to [`device_support.yml`](./device_support.yml) file and reference it in the Markdown page.

The documentation depends on additional Python code (such as referencing code/PR/issues) to later call them in Markdown, these functions are defined in [`docs/_misc/macros.py`](./docs/_misc/macros.py).

## Building

Mkdocs itself depends on Python, therefore a Python environment is required to get running the documentation.

Having a virtual/isolated Python environment is **strongly recommended** (and it is mandatory for scripts) to not break user/system-wide Python installations. One of ways to have per-project Python installations is installing [`rye`](https://rye.astral.sh/).

There is a [`tasks.sh`](./tasks.sh) bash script provided in the repository to make working with the documentation easier. Here are some useful commands;

Install dependencies:

```
$ ./tasks.sh deps deps_tools
```

`deps` will require having `rye` installed, because since `rye` doesn't expose `pip` it has own command to install and manage dependencies. If you use another Python version/package manager, calling `pip install .` (or setuptools-compatible tool) should be enough as the `pyproject.toml` file doesn't use non-standard keys.

`deps_tools` downloads few command line tools to current folder, which later used for minifying the generated documentation. (HTML & images) It is not required to have these tools to have documentation built, so you can omit this command (by simply running `./tasks.sh deps` alone) if you don't want to download them.

Preview documentation locally:

```
$ ./tasks.sh serve
```

Build documentation:

```
$ ./tasks.sh dist
```

Publish documentation:

```
$ ./tasks.sh publish
```

By default, it pushes the documentation to `pages` branch in [`Freeyourgadget/gadgetbridge_org`](https://codeberg.org/Freeyourgadget/gadgetbridge_org) repository.

To publish the documentation, it needs to be first built with `./tasks.sh dist`. The script will fail if there is no output folder created by `dist` task. To do the both, (build & deploy), simply run `./tasks.sh dist publish`.

## License

This documentation with its all content has provided under [GNU Affero General Public License (AGPLv3)](./LICENSE) unless written otherwise.

This documentation is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
