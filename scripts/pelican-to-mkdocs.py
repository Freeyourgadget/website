#!/usr/bin/env python3

#
#    Copyright (C) 2023 ysfchn
#
#    This file is part of Gadgetbridge.
#
#    Gadgetbridge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Gadgetbridge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
    A script that converts Pelican's frontmatter format in given
    Markdown files as a ZIP to YAML that Mkdocs can understand.
"""

from zipfile import ZipFile
from pathlib import Path
from typing import Dict
from datetime import datetime
import time

SUMMARY_CHAR = 500

# Name of the authors as appears in the blog.freeyourgadget.org,
# mapped to an key, which is later used in .authors.yml. 
AUTHOR_ID = {
    "Andreas Shimokawa": "ashimokawa",
    "ChatGPT": "chatgpt",
    "Petr Vaněk": "vanous",
    "Daniele Gobbetti": "danielegobbetti",
    "Gadgetbridge developers": "gadgetbridge"
}

def get_local_offset():
    # FIXME: unused
    # https://stackoverflow.com/a/3168394
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = - (time.altzone if is_dst else time.timezone)
    return utc_offset

def create_post(post : Dict[str, str]) -> str:
    return "\n".join([
        "---", "slug: {slug}",
        "date: {date}", "authors:",
        "{author}", "categories:",
        "{category}", "---", "",
        "# {title}", "", ""
    ]).format(
        date = \
            datetime.fromisoformat(post["date"]).isoformat(timespec = "seconds"),
        author = "  - " + "\n  - ".join(
            [AUTHOR_ID.get(x, "unknown") for x in post["author"]]
        ),
        title = post["title"],
        slug = post["slug"],
        category = "  - " + "\n  - ".join(post["category"])
    )

def convert_post(post : str):
    front, content = post.split("\n\n", maxsplit = 1)
    meta = {"author": [], "category": []}
    for i in front.split("\n"):
        title, value = i.strip().split(": ", maxsplit = 1)
        if title == "Title":
            meta["title"] = value
        elif title == "Slug":
            meta["slug"] = value
        elif title == "Date":
            meta["date"] = value
        elif title in ["Authors", "Author"]:
            meta["author"].append(value)
        elif title == "Category":
            meta["category"].append(value)
    # Create a summary line
    split = content.split("\n")
    char = 0
    for i, line in enumerate(split.copy()):
        char += len(line)
        if line:
            continue
        if char > SUMMARY_CHAR:
            split.insert(i, "<!-- more -->")
            break
    return create_post(meta) + "\n".join(split) \
        .replace("src=/images/", "src=../images/") \
        .replace(".html)", ".md)")

def convert_meta_zip(file : Path):
    posts_dir = Path(".") / "converted-blog-posts"
    posts_dir.mkdir(exist_ok = True)
    with ZipFile(file, "r") as zfile:
        for i in zfile.infolist():
            if i.filename.endswith(".md"):
                with zfile.open(i, "r") as opened:
                    name = i.filename.split("/")[-1]
                    print("Converting", name, "...")
                    post = convert_post(opened.read().decode("utf-8"))
                    with (posts_dir / name).open("w+") as f:
                        f.write(post)
            elif i.is_dir():
                continue
            else:
                filename = (posts_dir / i.filename.replace("sample/", ""))
                print("Copying", i.filename.split("/")[-1], "...")
                filename.parent.mkdir(parents = True, exist_ok = True)
                with filename.open("wb") as f:
                    f.write(zfile.open(i, "r").read())

def main():
    posts_zip = Path(".") / "posts.zip"
    if not posts_zip.exists():
        raise Exception(posts_zip.resolve().as_posix() + " doesn't exists.")
    convert_meta_zip(posts_zip)

if __name__ == "__main__":
    main()