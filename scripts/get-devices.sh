#!/usr/bin/env bash
set -euo pipefail

#
#    Copyright (C) 2025 ysfchn
#
#    This file is part of Gadgetbridge.
#
#    Gadgetbridge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Gadgetbridge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Read existing devices from "device_support.yml" file.
get_current_list() {
    python -c 'import yaml,sys,json;print(json.dumps(yaml.safe_load(sys.stdin)))' < \
    "$(realpath "$(dirname "${0}")/../device_support.yml")" |
        jq -Mrc '.device_support | to_entries[] | .key'
}

# Fetches a list of enum names and their coordinator
# class names from DeviceType.java.
list_enum_coordinators() {
    curl -fsS \
        "https://codeberg.org/Freeyourgadget/Gadgetbridge/raw/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/model/DeviceType.java" \
    | sed -rn "s/^ *(.*)\((.*)\.class\)[,;] *$/\1\t\2/p" \
    | sed -r "s/^VIVOMOVE_HR\t(.*)/GARMIN_VIVOMOVE_HR\t\1/" \
    | grep -oP "^(?!TEST)(?!UNKNOWN).*\t.*$"
}

# Fetches a list of device type keys along with their
# mapped display names from strings.xml.
list_device_names() {
    curl -fsS \
        "https://codeberg.org/Freeyourgadget/Gadgetbridge/raw/branch/master/app/src/main/res/values/strings.xml" \
    | sed -rn "s/^ *<string name=[\"']devicetype_(.*)[\"']>(.*)<\\/string> *$/\1\t\2/p" \
    | sed -r "s/^qhybrid\t/fossil_qhybrid\t/; s/^mykronoz_zetime\t/zetime\t/; s/^xiaomi_watch_lite\t/mi_watch_lite\t/;" \
    | grep -oP "^(?!test)(?!unknown).*\t.*$"
}

# Fetch both enum names and device names, sort them and
# merge the both list with each other.
get_remote_list() {
    list_device_names | jq -MRrcs --arg "coordinators" "$(list_enum_coordinators)" '
        ([$coordinators | split("\n")[] | split("\t") | if .[0] then (.[0] | ascii_downcase) else empty end] | sort_by(. | gsub("_"; ""))) as $coordinators
        | [([. | split("\n")[] | split("\t") | if .[0] then [(.[0] | ascii_downcase), .[1]] else empty end] | sort_by(.[0] | gsub("_"; ""))), $coordinators] | transpose[]
        | [
            (if ((.[0][0] | length) > (.[1] | length)) then .[0][0] else .[1] end),
            .[0][1]
        ] | @tsv
    '
}

help() {
    echo "usage: ${0} (list_current|list_remote)"
}

if [ "$#" -eq 0 ]; then
    help
    exit 1
fi

if [ "${1}" = "list_current" ]; then
    get_current_list
elif [ "${1}" = "list_remote" ]; then
    get_remote_list
else
    help
    exit 1
fi