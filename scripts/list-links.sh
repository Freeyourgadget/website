#!/usr/bin/env bash
set -euo pipefail

#
#    Copyright (C) 2025 ysfchn
#
#    This file is part of Gadgetbridge.
#
#    Gadgetbridge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Gadgetbridge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

TRUSTED_DOMAINS="
    f-droid.org
    codeberg.org
    freeyourgadget.codeberg.page
    github.com
    gitlab.com
    git-scm.com
    gist.github.com
    old.reddit.com
    termux.dev
    play.google.com
    en.wikipedia.org
    apt.izzysoft.de
    android.izzysoft.de
    catima.app
    osmand.net
    gpxsee.org
    home.openweathermap.org
    developer.android.com
    android.googlesource.com
    source.android.com
    sqlite.org
    stackoverflow.com
    unix.stackexchange.com
    tasker.joaoapps.com
    user.huami.com
    duckduckgo.com
    web.archive.org
    www.gnu.org
    pebblefw.s3.amazonaws.com
    gadgetbridge.org
    blog.freeyourgadget.org
    medium.com
    opentracksapp.com
    open-vsx.org
    espruino.com
    www.espruino.com
    www.bluetooth.com
    www.banglejs.com
    pine64.org
    www.wireshark.org
    arstechnica.com
    asteroidos.org
    archive.md
    hosted.weblate.org
"

# Iterate through all Markdown files in the documentation and search for HTTP(S) links with grep.
# Then, for each match, get the hostname of the link and check if it is defined in TRUSTED_DOMAINS list.
# Finally, create a TSV (tab-separated values) file containing the each found link and with the result
# if the link is from a "trusted" domain.
dump_links() {
    docs_path="$(realpath "$(dirname "${0}")/../docs")"
    grep --recursive --with-filename --line-number --only-matching --include "*.md" --exclude-dir "blog" \
        --extended-regexp --ignore-case "${docs_path}" --regexp "https?://[^>') ]+" \
    | jq -MRcsr --arg "trusted_domains" "${TRUSTED_DOMAINS}" '
        [($trusted_domains | split("\n")[] | if (. | length) > 0 then gsub(" "; "") else empty end)] as $trusted_domains 
        | [["domain", "is_trusted_domain", "file", "lineno", "link"]] + ([split("\n")[] | split(":") 
        | (.[2:] | join(":")) as $link 
        | ($link | sub("^https?://(?<hostname>[^/]+).*"; "\(.hostname)")) as $domain
        | if .[0] | not then empty else [
            $domain, ($trusted_domains | index($domain)) != null, .[0], .[1], $link
        ] end] | sort_by(.[0])) | .[] | @tsv
    ' | sed -e "s#\t${docs_path}/#\t#"
}

dump_links