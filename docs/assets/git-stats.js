/*
    Copyright (C) 2023 ysfchn

    This file is part of Gadgetbridge.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function retrieveLatestTag() {
    return new Promise((resolve, reject) => {
        fetch("https://codeberg.org/api/v1/repos/Freeyourgadget/Gadgetbridge/tags?page=1&limit=1").then(
            (resp) => !resp.ok ? reject(new Error("Couldn't obtain repository details.")) : resp.json()
        ).then((data) => resolve({
            tag: data[0]["name"],
            time: data[0]["commit"]["created"],
            sha: data[0]["commit"]["sha"]
        })).catch((reason) => reject(reason))
    })
}

document$.subscribe(() => {
    /*
        Fetch the latest version of Gadgetbridge and put under the 
        git repository name in navigation bar. 
        
        Material for Mkdocs normally have this feature, but it only exists 
        for Github and Gitlab repositories, so we mimic the same feature in here
        by ourselves.
    */
    const gitStats = document.querySelector("[data-md-component='source'] .md-source__repository");

    function loadInfo(data) {
        const facts = document.createElement("ul");
        facts.className = "md-source__facts";
        const stat = document.createElement("li");
        stat.className = "md-source__fact md-source__fact--other";
        stat.innerText = data["gb"]?.["tag"];
        if (stat.innerText) {
            facts.appendChild(stat);
            gitStats.appendChild(facts);
        }
    }

    function fetchInfo() {
        retrieveLatestTag().then((release) => {
            __md_set("__git_repo", {"gb": release}, sessionStorage);
            loadInfo({"gb": release});
        });
    }

    if (!document.querySelector('[data-md-component="source"] .md-source__facts')) {
        const cached = __md_get("__git_repo", sessionStorage);
        if ((cached != null) && (cached["gb"])) {
            loadInfo(cached);
        } else {
            fetchInfo();
        }
    }
});