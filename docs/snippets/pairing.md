<!-- --8<-- [start:auth_key_first] -->

Before entering the key and attempting to connect, you should remove all permissions from the vendor app or uninstall it, since it can conflict with Gadgetbridge. **Do not unpair the watch from the vendor app**, as that will invalidate the auth key.

If you are currently on the "Device discovery" page, long press on the device name and click on "Auth key" to enter the key.

If you added the gadget already, touch the :material-cog: icon under your gadget name (while on the home screen) to go to the "Device specific settings", open the "Authentication" section, and click on "Auth key" to enter the key.

<!-- --8<-- [end:auth_key_first] -->

<!-- --8<-- [start:auth_key_hex] -->

Enter your key prefixed with `0x` ( eg. if your key is `46fec12c98382dgf` enter `0x46fec12c98382dgf`). Don't press Enter/Return key on the keyboard and make sure `0x` is really `0x` before pressing "OK", as some keyboards might use different symbols.

<!-- --8<-- [end:auth_key_hex] -->

<!-- --8<-- [start:auth_key_finish] -->

![](../../assets/static/screenshots/auth_key.jpg){: height="600" style="height: 600px;" }

After entering your authentication key, try connecting to your gadget to see if it is working.

If everything works alright, congratulations! Now you can uninstall the vendor app of your gadget and start using Gadgetbridge.

[:material-arrow-right: Configuring your gadget](../features/index.md){: .gb-button }

<!-- --8<-- [end:auth_key_finish] -->
