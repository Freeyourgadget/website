---
title: Pebble
---

# Pebble

See [About Pebble gadgets](../../basics/pairing/pebble.md) for instructions to pair Pebble gadgets.

!!! success "PebbleOS is now open-source"
    The current owner of Pebble; Google, [has announced](https://opensource.googleblog.com/2025/01/see-code-that-powered-pebble-smartwatches.html){: target="_blank" } that they have released the [source of PebbleOS](https://github.com/google/pebble){: target="_blank" }, the firmware running in Pebble wearables on January 27, 2025.

    Related post from a Pebble founder: [Why We're Bringing Pebble Back](https://ericmigi.com/blog/why-were-bringing-pebble-back){: target="_blank" }


???+ tip "Resources for Pebble"
    * Third party wikis and manuals
        * [Pebble Time Steel, Pebble Time Round und Pebble Time - Anleitung und Handbuch][source-1]{: target="_blank" } (German)
        * [Reddit Pebble Wiki][source-2]{: target="_blank" }
    * Some interesting pages around Pebble and privacy
        * [Pebble ist tot. Lang lebe die Pebble!][source-3]{: target="_blank" } (12/2016; German article on Gadgetbridge)
        * [Smartwatches and privacy - contradiction in terms?][source-4]{: target="_blank" } (6/2016; English and German)
        * [Personal Privacy, Smartwatch Security, and You][source-5]{: target="_blank" } (10/2014)
        * [Privacy concerns with new Pebble privacy policy][source-6]{: target="_blank" } (8/2015)
        * [Pebble Privacy Policy (archived)][source-7]{: target="_blank" }
        * [Companion apps and (no) security model][source-8]{: target="_blank" }
    * Other useful links
        * [Pebblestuff][source-9]{: target="_blank" } introduces apps, watchfaces, accessories & more
        * ~~[CloudPebble][source-10]{: target="_blank" }~~ develop online; was requiring a Pebble account, now it is down
        * [The best apps for Pebble Time and Pebble Time Round][source-11]{: target="_blank" }

[source-1]: https://web.archive.org/web/20211205074324/https://www.appdated.de/2016/03/pebble-time-steel-pebblet-time-round-und-pebble-time-anleitung-und-handbuch/
[source-2]: https://old.reddit.com/r/pebble/wiki/index
[source-3]: https://www.androidpit.de/pebble-ist-tot-lang-lebe-die-pebble
[source-4]: https://android.izzysoft.de/articles/named/smartwatch-privacy
[source-5]: https://smartwatches.org/learn/privacy-smartwatch-security-and-you/
[source-6]: https://old.reddit.com/r/pebble/comments/3hsxez/privacy_concerns_with_new_pebble_privacy_policy/
[source-7]: http://web.archive.org/web/20170705022758/https://www.pebble.com/legal/privacy/
[source-8]: https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/302#issuecomment-219211974
[source-9]: https://www.pebblestuff.io/
[source-10]: https://cloudpebble.net/
[source-11]: https://web.archive.org/web/20160608105127/https://www.wareable.com/apps/the-best-apps-for-pebble-and-pebble-steel

## Classic {{ device("pebble_classic") }}

## Steel {{ device("pebble_steel") }}

## Time {{ device("pebble_time") }}

## Time Steel {{ device("pebble_time_steel") }}

## Time Round {{ device("pebble_time_round") }}

## Classic 2 {{ device("pebble_classic_2") }}
