---
title: Huawei/Honor
---

# Huawei/Honor

See the [Huawei/Honor gadgets](../../basics/topics/huawei-honor.md) for a list of features and issues common to all Huawei and Honor watches.

## Honor Band 3 {{ device("honor_band_3") }}

## Honor Band 4 {{ device("honor_band_4") }}

## Honor Band 5 {{ device("honor_band_5") }}

## Honor Band 6 {{ device("honor_band_6") }}

## Honor Band 7 {{ device("honor_band_7") }}

Support for this band is still experimental and not yet confirmed to be working.

## Honor MagicWatch 2 {{ device("honor_magicwatch_2") }}

Support for this watch was added in {{ 3541|pull }}.

## Honor Watch GS 3 {{ device("honor_watch_gs_3") }}

Support for this watch was added in {{ 4023|pull }}.

## Honor Watch GS Pro {{ device("honor_watch_gs_pro") }}

Support for this watch was added in {{ 4104|pull }}.

## Huawei Band 2 {{ device("huawei_band_2") }}

Support for this watch was added in {{ 4296|pull }}.

## Huawei Band 2 Pro {{ device("huawei_band_2_pro") }}

Support for this gadget was added in {{ 4296|pull }}, without access to the device. It should work, but was not tested at all. If you confirm it works or face any issues, please [open an issue]({{ constants.bug_report }}){: target="_blank" } or reach out on [Matrix]({{ constants.matrix }}){: target="_blank" }.

## Huawei Band 3e {{ device("huawei_band_3e") }}

## Huawei Band 3 Pro {{ device("huawei_band_3_pro") }}

Support for this watch was added in {{ 4296|pull }}.

## Huawei Band 4 {{ device("huawei_band_4") }}

## Huawei Band 4 Pro {{ device("huawei_band_4_pro") }}

## Huawei Band 4e {{ device("huawei_band_4e") }}

## Huawei Band 6 {{ device("huawei_band_6") }}

## Huawei Band 7 {{ device("huawei_band_7") }}

## Huawei Band 8 {{ device("huawei_band_8") }}

## Huawei Band 9 {{ device("huawei_band_9") }}

Support for this watch was added in {{ 3771|pull }}.

Some issues were found with newer firmware versions, see {{ 3784|issue }} for details.

## Huawei Talk Band B6 {{ device("huawei_talk_band_b6") }}

## Huawei Watch 3 {{ device("huawei_watch_3") }}

Support for this watch was added in {{ 3922|pull }}.

## Huawei Watch 3 Pro {{ device("huawei_watch_3_pro") }}

Support for this watch was added in {{ 3922|pull }}.

## Huawei Watch 4 {{ device("huawei_watch_4") }}

Support for this watch was added in {{ 4157|pull }}.

Known issues:

* Activity sync is not working - {{ 3814|issue }}

## Huawei Watch 4 Pro {{ device("huawei_watch_4_pro") }}

Support for this watch was added in {{ 3754|pull }}.

Known issues:

* Activity sync is not working - {{ 3814|issue }}

## Huawei Watch D2 {{ device("huawei_watch_d2") }}

Support for this watch was added in {{ 4238|pull }}.

## Huawei Watch GT {{ device("huawei_watch_gt") }}

## Huawei Watch GT 2 {{ device("huawei_watch_gt_2") }}

## Huawei Watch GT 2 Pro {{ device("huawei_watch_gt_2_pro") }}

Support for this watch is still very early and not working properly.

## Huawei Watch GT 2e {{ device("huawei_watch_gt_2e") }}

Support for this watch is still very early and not working properly.

## Huawei Watch GT 3 {{ device("huawei_watch_gt_3") }}

## Huawei Watch GT 3 Pro {{ device("huawei_watch_gt_3_pro") }}

## Huawei Watch GT 3 SE {{ device("huawei_watch_gt_3_se") }}

Support for this watch was added in {{ 4163|pull }}.

## Huawei Watch GT 4 {{ device("huawei_watch_gt_4") }}

Support for this watch was added in {{ 4157|pull }}.

## Huawei Watch GT 5 {{ device("huawei_watch_gt_5") }}

Support for this watch was added in {{ 4155|pull }}.

## Huawei Watch GT 5 Pro {{ device("huawei_watch_gt_5_pro") }}

Support for this watch was added in {{ 4155|pull }}.

## Huawei Watch GT Cyber {{ device("huawei_watch_gt_cyber") }}

Support for this watch was added in {{ 4204|pull }}.

## Huawei Watch GT Runner {{ device("huawei_watch_gt_runner") }}

Support for this watch was added in {{ 3956|pull }}.

## Huawei Watch Fit {{ device("huawei_watch_fit") }}

Support for this watch was added in {{ 3574|pull }}.

## Huawei Watch Fit 2 {{ device("huawei_watch_fit_2") }}

Support for this watch was added in {{ 3745|pull }}.

## Huawei Watch Fit 3 {{ device("huawei_watch_fit_3") }}

Support for this watch was added in {{ 3771|pull }}.

## Huawei Watch Ultimate {{ device("huawei_watch_ultimate") }}

Support for this watch was added in {{ 3636|pull }}.
