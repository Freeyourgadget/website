---
title: Lenovo
---

# Lenovo

<!-- TODO: https://codeberg.org/mamutcho/Gadgetbridge/wiki#user-content-supported-watches-and-status -->

## Watch 9 {{ device("lenovo_watch_9") }}

Support for the Lenovo Watch 9 was added in {{ 1190|pull }}.

Implemented features:

* Notifications
* Alarms (not heavily tested)
* Setting step goal (progress can be checked on the watch face)
* Time calibration/sync

## Watch X {{ device("lenovo_watch_x") }}

## Watch X Plus {{ device("lenovo_watch_x_plus") }}
