---
title: Hama
---

# Hama


## Fit6900 {{ device("hama_fit6900") }}

Support for this gadget was added in {{ 3832|pull }}.

### Supported features

- Set time
- Set language
- Firmware version
- User data (age, weight, height, steps goal)
- Alarms
- Do not disturb
- Drink reminder
- Heart rate measurement (if enabled, sleep monitoring becomes active)
- Wrist gesture on/off
- Notifications
    - Incoming call notification + reject call
- Find device
- Find phone
- Media player remote
- Camera remote

### Known issues

* Battery status is not displayed
    * Code is implemented, but disabled, since the watch always reports the same value

### Missing features

* Handling of heart rate/steps/sleep/sport activities realtime data and sync
