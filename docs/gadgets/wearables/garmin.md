---
title: Garmin
---

# Garmin

Gadgetbridge supports some Garmin devices, listed below. It's very likely that other devices are supported, you can try to use [Pairing unsupported gadgets](../../basics/pairing/index.md#pairing-unsupported-gadgets) to pair an unsupported Garmin watch. If it works, please [open an issue]({{ constants.device_request}}){: target="_blank" } or reach out on [Matrix]({{ constants.matrix }}){: target="_blank" } so we can enable official support for it.

See the [Garmin](../../basics/topics/garmin.md) page for a list of features and issues common to all Garmin watches.

!!! warning "Warning"
    * We have noticed that in some cases the Bluetooth traffic contains some bits of information (`oauth_token`, `oauth_consumer_key`, `oauth_signature`, ...) that might (potentially, we did not test) grant access to the user's garmin account. **Therefore we warn all the users to mind this information when sharing the logs.**
    * User data/configuration is unmanaged, which means that whatever was set in the official app will remain untouched, no matter what is set in Gadgetbridge.

### Enduro 3 {{ device("garmin_enduro_3") }}

Added based on feedback from {{ 4122|issue }}.

### Epix Pro {{ device("garmin_epix_pro") }}

Added based on feedback from {{ 3810|issue }}.

### Fenix 5 {{ device("garmin_fenix_5") }}

Added based on feedback from {{ 3869|issue }}.

### Fenix 5 Plus {{ device("garmin_fenix_5_plus") }}

Added based on feedback from Matrix.

This device might have issues pairing if not connected at least once to the official app, and is therefore marked as experimental - see {{ 3963|issue }}.

### Fenix 5X Plus {{ device("garmin_fenix_5x_plus") }}

Added based on feedback from Matrix.

This device might have issues pairing if not connected at least once to the official app, and is therefore marked as experimental - see {{ 3963|issue }}.

### Fenix 6 {{ device("garmin_fenix_6") }}

Added based on feedback from {{ 3624|pull }}.

### Fenix 6 Sapphire {{ device("garmin_fenix_6_sapphire") }}

Added based on feedback from {{ 3624|pull }}.

### Fenix 6S Pro {{ device("garmin_fenix_6s_pro") }}

Added based on feedback from Mastodon.

### Fenix 6S Sapphire {{ device("garmin_fenix_6s_sapphire") }}

Added based on feedback from Matrix.

### Fenix 6X Pro Solar {{ device("garmin_fenix_6x_pro_solar") }}

Added based on feedback from {{ 4416|issue }}.

### Fenix 7 {{ device("garmin_fenix_7") }}

Added based on feedback from {{ 4267|issue }}.

### Fenix 7S {{ device("garmin_fenix_7s") }}

Added based on feedback from Matrix.

### Fenix 7S Pro {{ device("garmin_fenix_7s_pro") }}

--8<-- "blocks.md:non_released_gadget"

Added based on feedback from {{ 4488|issue }}.

### Fenix 7X {{ device("garmin_fenix_7x") }}

Added based on feedback from {{ 4395|issue }}.

### Fenix 7 Pro {{ device("garmin_fenix_7_pro") }}

Added based on feedback from Matrix.

### Fenix 8 {{ device("garmin_fenix_8") }}

Added based on feedback from {{ 4226|issue }}.

### Forerunner 45 {{ device("garmin_forerunner_45") }}

--8<-- "blocks.md:non_released_gadget"

Added based on feedback from {{ 4472|issue }}.

### Forerunner 55 {{ device("garmin_forerunner_55") }}

Added based on feedback from {{ 4304|issue }}.

### Forerunner 165 {{ device("garmin_forerunner_165") }}

Added based on feedback from {{ 4074|issue }}.

### Forerunner 235 {{ device("garmin_forerunner_235") }}

Added based on feedback from {{ 4285|issue }}.

### Forerunner 245 {{ device("garmin_forerunner_245") }}

### Forerunner 245 Music {{ device("garmin_forerunner_245_music") }}

Added based on feedback from {{ 3986|issue }}.

This device might have issues pairing if not connected at least once to the official app, and is therefore marked as experimental - see {{ 3963|issue }}.

### Forerunner 255 {{ device("garmin_forerunner_255") }}

Added based on feedback from {{ 3624|pull }}.

### Forerunner 255 Music {{ device("garmin_forerunner_255_music") }}

Added in {{ 3940|pull }}.

### Forerunner 255S {{ device("garmin_forerunner_255s") }}

Added based on feedback from {{ 3841|issue }}.

### Forerunner 255S Music {{ device("garmin_forerunner_255s_music") }}

Added in {{ 3932|pull }}.

### Forerunner 265 {{ device("garmin_forerunner_265") }}

Added based on feedback from {{ 3831|issue }}.

### Forerunner 265S {{ device("garmin_forerunner_265s") }}

Added based on feedback from {{ 4131|issue }}.

### Forerunner 620 {{ device("garmin_forerunner_620") }}

Added based on feedback from {{ 4292|issue }}.

### Forerunner 735XT {{ device("garmin_forerunner_735xt") }}

--8<-- "blocks.md:non_released_gadget"

Added based on feedback from {{ 4562|issue }}.

Support is not fully confirmed, and it was reported that activity sync gets stuck, so it is marked as experimental.

### Forerunner 955 {{ device("garmin_forerunner_955") }}

Added in {{ 4125|pull }}.

### Forerunner 965 {{ device("garmin_forerunner_965") }}

Added based on feedback from {{ 3959|issue }}.

It has been reported that this watch sometimes does not advertise the expected bluetooth name, so you might face issues pairing. If Gadgetbridge is unable to find your watch, please try to follow the [pairing unsupported gadgets](../../basics/pairing/index.md#pairing-unsupported-gadgets) instructions, and select the "Garmin Forerunner 965".

If you face any issues, please [open an issue]({{ constants.bug_report }}){: target="_blank" } or reach out on [Matrix]({{ constants.matrix }}){: target="_blank" }.

### Instinct 2 {{ device("garmin_instinct_2") }}

Added in {{ 4333|pull }}.

### Instinct 2 Solar {{ device("garmin_instinct_2_solar") }}

### Instinct 2 Solar - Tactical Edition {{ device("garmin_instinct_2_soltac") }}

### Instinct 2S {{ device("garmin_instinct_2s") }}

### Instinct 2S Solar {{ device("garmin_instinct_2s_solar") }}

Added in {{ 3805|pull }}.

### Instinct 2X Solar {{ device("garmin_instinct_2x_solar") }}

Added based on feedback from {{ 3063|issue }}.

### Instinct 3 {{ device("garmin_instinct_3") }}

--8<-- "blocks.md:non_released_gadget"

Added based on feedback from {{ 4601|pull }}.

### Instinct Crossover {{ device("garmin_instinct_crossover") }}

### Instinct E {{ device("garmin_instinct_e") }}

--8<-- "blocks.md:non_released_gadget"

Added based on feedback from {{ 4526|issue }}.

### Lily 2 Active {{ device("garmin_lily_2_active") }}

--8<-- "blocks.md:non_released_gadget"

Added based on feedback from {{ 4552|issue }}.

### Instinct {{ device("garmin_instinct") }}

Added based on feedback from Matrix.

### Instinct Solar {{ device("garmin_instinct_solar") }}

Also known as "Instinct Dual Power" in some markets.

### Swim 2 {{ device("garmin_swim_2") }}

### Venu {{ device("garmin_venu") }}

Added based on feedback from {{ 4003|issue }}.

### Venu Sq {{ device("garmin_venu_sq") }}

Added based on feedback from {{ 4228|issue }}.

### Venu Sq 2 {{ device("garmin_venu_sq_2") }}

Added based on feedback from Matrix.

### Venu 2 {{ device("garmin_venu_2") }}

Added based on feedback from {{ 3835|issue }}.

### Venu 2S {{ device("garmin_venu_2s") }}

Added based on feedback from {{ 4010|issue }}.

### Venu 2 Plus {{ device("garmin_venu_2_plus") }}

### Venu 3 {{ device("garmin_venu_3") }}

This watch supports real-time settings and sleep stages.

### Venu 3S {{ device("garmin_venu_3s") }}

Added based on feedback from {{ 3602|issue }}.

### Vívoactive 3 {{ device("garmin_vivoactive_3") }}

Added based on feedback from Matrix.

### Vívoactive 4 {{ device("garmin_vivoactive_4") }}

### Vívoactive 4s {{ device("garmin_vivoactive_4s") }}

### Vívoactive 5 {{ device("garmin_vivoactive_5") }}

### Vívosmart 5 {{ device("garmin_vivosmart_5") }}

Added based on feedback from {{ 3269|issue }}.

### Vívomove HR {{ device("garmin_vivomove_hr") }}

Support for this gadget was originally added in {{ 3180|pull }}. However, due to the necessary changes for newer devices to work, this support was replaced in {{ 3782|pull }}, which requires a firmware upgrade.

Data synced while using the old version is still in Gadgetbridge database, but will not be displayed in the UI. If you own a vivomove HR and were using Gadgetbridge with it before these changes, please [open an issue]({{ constants.bug_report }}){: target="_blank" } or reach out to us on [Matrix]({{ constants.matrix }}){: target="_blank" } in the main chat room.

### Vívomove Style {{ device("garmin_vivomove_style") }}

### Vívomove Trend {{ device("garmin_vivomove_trend") }}

Added based on feedback from {{ 3875|issue }}.

### Vívosport {{ device("garmin_vivosport") }}

Added in {{ 3892|pull }}.
