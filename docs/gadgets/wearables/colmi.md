---
title: Colmi
---

# Colmi

## R02 {{ device("colmi_r02") }}

Support for this line of cheap smart rings was added with {{ 3896|pull }}. The official app for this hardware, QRing, supports many other cheap smart rings as well. Those are expected to be supported by Gadgetbridge as well, so if you come across one, please let us know.

!!! warning "Other hardware with the same name"
    There have been reports online of Colmi R02/R03/R06 rings with different hardware and a different companion app. Those rings are **not** supported by Gadgetbridge. This support only applies to rings supported by QRing.

Tested with firmware versions: 3.00.06, 3.00.10, 3.00.17.

Features supported by Gadgetbridge:

* Battery level
* Device configuration
    * HR measurement interval
    * Stress measurement toggle
    * SpO2 measurement toggle
* Synchronize, persist and display historical data
    * Steps
    * Heart rate
    * Sleep (deep, light, REM, awake)
    * SpO2
    * Stress level
    * Temperature measurements (R09 only)
* Find device (turn on green LED for 10 seconds)
* Power off device
* Manual one-off HR measurement

Currently missing features:

* Firmware upgrade
* Camera shutter gesture
* HRV measurements (firmware 3.00.10+)

## R03 {{ device("colmi_r03") }}

This device has the same hardware as the [Colmi R02](#device__colmi_r02), so those details also apply to this one.

## R06 {{ device("colmi_r06") }}

This device has the same hardware as the [Colmi R02](#device__colmi_r02), so those details also apply to this one.

## R09 {{ device("colmi_r09") }}

This device has the largely same hardware as the [Colmi R02](#device__colmi_r02), so those details also apply to this one.

In addition, this device supports temperature measurements and gestures. Support for gestures is not implemented in Gadgetbridge yet.

## R10 {{ device("colmi_r10") }}

This device has the same hardware as the [Colmi R02](#device__colmi_r02), so those details also apply to this one.
