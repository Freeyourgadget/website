---
title: Bangle.js
---

# Bangle.js

> Bangle.js support came directly from Gordon Williams of Espruino! And marks a special milestone for Gadgetbridge. It is the first time that Gadgetbridge support was contributed directly from the creators of a device! The Bangle.js is a fully hackable smartwatch with an open source firmware and apps written in JavaScript. For more information visit banglejs.com.

Source: [Freeyourgadget blog](https://blog.freeyourgadget.org/release-0_40_0.html){: target="_blank" }

!!! warning "There is an another Gadgetbridge app made for Bangle.js"
    With the standard Gadgetbridge app you can manage your Bangle.js devices, but you can't get information from the internet through watch because the standard app doesn't have internet permission. So, you might want to use ["Bangle.js Gadgetbridge" app instead.](https://www.espruino.com/Gadgetbridge){: target="_blank" }

Apart from Bangle.js watches, any device that supports the Nordic UART service and can decode JSON packets (like Bluetooth LE Espruino or Adafruit Bluefruit) is also supported with this implementation.

## Bangle.js 1 {{ device("bangle_js_1") }}

## Bangle.js 2 {{ device("bangle_js_2") }}
