---
title: IKEA
---

## Idasen {{ device("ikea_idasen") }}

Support for the IKEA Idasen desk controller was added in {{ 4234|pull }}.

Implemented features:

* Move the desk up and down manually
* See the current height and speed
* Store three desk positions
* Move to any of the stored desk positions
