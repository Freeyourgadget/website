---
title: Marstek
---

## B2500 {{ device("marstek_b2500") }}

Support for the Marstek B2500 solar battery was added in {{ 4408|pull }}.

Tested with firmware V220.

Implemented features:

* setting the time
* getting the battery charge level
* getting temperature values
* setting/getting the minimum charge of the battery that shall remain
* setting/getting pass-through mode
* setting/getting automatic/manual mode
* setting/getting 5 time intervals with output wattage and enable/disable switch
* getting the firmware version
* reboot
