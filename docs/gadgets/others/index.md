---
title: Others
icon: material/chip
search:
  exclude: true
---

# Others

This category contains miscellaneous devices that don't fit in another category.

{{ file_listing() }}
