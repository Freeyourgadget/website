---
title: Gree
---

## Air Conditioner {{ device("gree_ac") }}

--8<-- "blocks.md:non_released_gadget"

Gadgetbridge is able to connect to Gree Air Conditioner units that have bluetooth, and configure them to connect to a Wi-Fi network, bypassing the need for the official app.

![](../../assets/static/screenshots/gadget/gree_pair_1.png){: height="600" style="max-height: 600px;" }

Once configured, the bind key is displayed, which can be used to configure the units in Home Assistant or other tools.

![](../../assets/static/screenshots/gadget/gree_pair_2.png){: height="600" style="max-height: 600px;" }

It is necessary to provide the hostname of a server in the local network that the AC units will connect to - see the [gree-dummy-tls-server](https://codeberg.org/joserebelo/gree-dummy-tls-server) project for more information.

It is **not possible** to control the AC units using Gadgetbridge - after being configured to connect to a Wi-Fi network, the bluetooth connection is shutdown and can't be used anymore.
