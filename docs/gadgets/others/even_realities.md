---
title: Even Realities
---

# Even Realities

Even Realities is a manufacturer of smart glasses: [Website](https://www.evenrealities.com/)

## G1 Smart Glasses {{ device("even_realities_g1") }}

The G1 glasses have a monocromatic display in the lenses that can perform many of the same functions as a smart watch.
The glasses utilize two seperate BLE connections, one for each lens/arm of the glasses.
The missing features are listed in the order of priority of being added.

#### Features supported by Gadgetbridge:

* Battery level
* Device bonding

#### Currently missing features:

* Clock syncing (the time will be wrong if not connected to the stock app first)
* Notifications
* Charging state
* Case battery status
* Calendar
* Weather
* Device Configurations (wear detection, head up angle)
    * Wear Detection
    * Headup angle
    * Remapping touch inputs
    * Display depth
    * Display veritcal location
* Configuring the dashboard and widgets
* Navigation
* Firmware Update
* Widgets (quick notes, stocks)
* Voice control features
    * Creating quick notes
    * Teleprompter
    * Any sort of AI integration (this may never be added)

#### Supported Hardware Revisions:

* G1A - Round frame version of the glasses
* G1B - Square frame version of the glasses

#### Tested with firmware versions:

* 1.4.5
* 1.5.0
* 1.5.2

### Changelog

* {{ 4553|pull }} - Inital support was added. Can pair, connect and show the battery life.
* {{ 4621|pull }} - Enhanced support so glasses are shown as a single device instead of two seperate ones.
