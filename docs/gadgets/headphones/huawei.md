---
title: Huawei
---

## FreeBuds 5i {{ device("huawei_freebuds_5i") }}

Support for this gadget was added in {{ 4366|pull }}.

Supported features:

* Reading of the three battery values
* Reading the firmware and hardware version
* Automatic pause of playback can be activated/deactivated
* Switching between the audio profiles:
    * ANC
    * Transparent / Awareness
    * OFF

Gadgetbridge-specific features for headphones:

* Automatically answer phone calls after a configurable delay
* Speak notifications aloud

Not supported:

* Find device
