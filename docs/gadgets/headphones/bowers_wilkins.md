---
title: Bowers & Wilkins
---

## P Series {{ device("bowers_wilkins_p_series") }}

Support for this gadget was added in {{ 4288|pull }}.

Only tested with the Pi5 S2.

Supported features:

* Read battery (earphones, case)
* Read firmware info
* Read device name
* Active Noise Cancelling (on/off)
* Passthrough level (0-2, where 0 is off)
* Wear sensor toggle
