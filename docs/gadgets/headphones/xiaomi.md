---
title: Xiaomi
---

## Redmi Buds 4 Active {{ device("xiaomi_redmi_buds_4_active") }}

Support for this gadget was added based on feedback from {{ 4359|issue }}.

Supported features:
* Connection and authentication
* Firmware version
* Battery percentage

Missing features:
* Low latency mode

## Redmi Buds 5 Pro {{ device("xiaomi_redmi_buds_5_pro") }}

Support for this gadget was added in {{ 4343|pull }}.

Supported features:

* Connection and authentication
* Firmware version
* Battery percentage
* Noise cancelling settings
    * Switch between ANC/Transparency/OFF
    * Relative strength settings
    * Adaptive ANC toggle
* Configuring all touch options
* Ear detection play/pause
* Auto call answer
* Double connection
* Adaptive sound settings toggles
* Equalizer presets and custom curve

Not working:

* Personalized ANC
    * code for toggle is present, but commented-out, as the logic for ear calibration is missing
* Spatial Audio
* Find my earbuds
* Firmware Update

## Redmi Buds 6 Active {{ device("xiaomi_redmi_buds_6_active") }}

--8<-- "blocks.md:non_released_gadget"

Support for this gadget was added in {{ 4449|pull }}.

Only battery percentage for headphones and case is implemented. Features that should be supported are currently unknown.
