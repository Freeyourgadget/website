---
title: Anker
---

## Soundcore Liberty 3 Pro {{ device("anker_soundcore_liberty_3_pro") }}

Support for this gadget was added in {{ 3753|pull }}.

### Working features

* Connection
* Firmware version and serial number
* Display battery life when sent by earbuds
* Set noise cancelling and transparency
* Update noise cancelling / transparency setting in Gadgetbridge when changed on the earbuds
* Set the behavior of touch actions

### Missing features

* Hear-Id (calibration of ANC for your ears)
* Equalizer
* Actively requesting the battery-life of the earbuds, so it may get out-of-date

## Soundcore Liberty 4 NC {{ device("anker_soundcore_liberty_4_nc") }}

Support for this gadget was added in {{ 4050|pull }}.

The details provided for [Soundcore Liberty 3 Pro](#device__anker_soundcore_liberty_3_pro) also apply to these headphones.

## Soundcore Q30 {{ device("anker_soundcore_q30") }}

--8<-- "blocks.md:non_released_gadget"

Support for this gadget was added in {{ 4316|pull }}.

### Working features

* Reading and writing ANC Settings
* Reading Battery at Connection in 20% Steps

What is not supported:

* Battery percentage updates while connected
* Equalizer (protocol is implemented, but no preference yet)
