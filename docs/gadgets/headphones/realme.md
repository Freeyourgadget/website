---
title: Realme
---

## Buds T110 {{ device("realme_buds_t110") }}

Support added based on feedback from Matrix. The protocol is the same as Oppo headphones. Tested with firmware 1.1.0.75.

Supported features:

* Read Firmware
* Read battery level (earbuds, case)
* Touch options per earbud (off, play/pause, previous track, next track, volume up, volume down, voice assistant)
    * Double tap
    * Triple tap
    * Long press
    * Long press both earbuds for game mode

Gadgetbridge-specific features for headphones:

* Automatically answer phone calls after a configurable delay
* Speak notifications aloud

Missing features:

* Sound effects (Bass Boost +, Balanced, Bright)
* Volume enhancer
* Game mode from app
