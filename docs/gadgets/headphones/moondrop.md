---
title: Moondrop
---

## Space Travel {{ device("moondrop_space_travel") }}

Support for this gadget was added in {{ 3857|pull }}.

Supported features:

* Equalizer profile
* Touch actions

Not supported:

* Battery percentage (it is still visible in Android itself)
