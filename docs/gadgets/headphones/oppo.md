---
title: Oppo
---

## Enco Air {{ device("oppo_enco_air") }}

Tested with firmware 160.160.827.

Supported features:

* Read Firmware
* Read battery level (earbuds, case)
* Touch options per earbud
    * Double tap (off, play/pause, previous track, next track, voice assistant)
    * Triple tap (off, voice assistant, game mode)
    * Long press (off, volume up, volume down)

Gadgetbridge-specific features for headphones:

* Automatically answer phone calls after a configurable delay
* Speak notifications aloud

## Enco Air2 {{ device("oppo_enco_air2") }}

Added based on feedback from {{ 4351|issue }}.

Should support the same features as the Enco Air.

Missing features:

* Read Firmware
* Enable/disable Game Mode from Gadgetbridge
* Switch equalizer presets (Original sound, Bass boost, Clear vocals)
