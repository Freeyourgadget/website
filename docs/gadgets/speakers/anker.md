---
title: Anker
---

## Soundcore Motion 300 {{ device("anker_soundcore_motion_300") }}

Support for this gadget was added in {{ 3968|pull }}.

### Supported features

* Battery reporting
* Power off
* Enable/disable voice prompts
* Set the button brightness
* Set the auto power off duration
* Enable/disable the LDAC codec
* Enable/disable adaptive direction for the EQ
* Set the EQ preset or choose a custom EQ
* Configure the bands of the parametric EQ for each of the 3 supported directions (standing, lying, hanging)
