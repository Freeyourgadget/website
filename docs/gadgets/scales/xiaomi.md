---
title: Xiaomi
---

# Xiaomi

## Body Composition Scale 2 {{ device("xiaomi_body_composition_scale_2") }}

Support for this scale has been added in {{ 1408|pull }}.

Real-time body weight measurements are persisted to the database and displayed in the charts.

Missing features:

* Querying previously measured weights stored by the scale
* Set the weight unit

## Smart Scale 2 {{ device("xiaomi_smart_scale_2") }}

Support for this scale has been added in {{ 3985|pull }}.

Supported features:

* Body weight measurement (including querying the previously measured weights stored by the scale)
* Set the weight unit (kilograms, pounds, jin)
* Allow/prevent measurement of small objects (<10 kg) from being added to the history
* Clear the weight history by performing a factory reset (in the debug menu)
