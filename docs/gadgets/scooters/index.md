---
title: Scooters
icon: material/scooter
search:
  exclude: true
---

# Scooters

This category contains scooters.

{{ file_listing() }}
