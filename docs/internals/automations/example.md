---
title: Automation example
---

# Automation example

With [Device actions](./events.md) you can broadcast a message (Intent) that can later be picked up with any automation app. This allows you to do any action that you want with these external automation apps. Gadgetbridge doesn't control and limit what you can do, instead, you rely on the third party automation applications themselves.

## With "Automate"

This example uses the [Automate](https://play.google.com/store/apps/details?id=com.llamalab.automate){: target="_blank" } app to send a notification when you woke up while wearing the gadget and connected to Gadgetbridge.

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_actions_2.jpg){: height="600" style="height: 600px;" }
<div markdown>

Before getting started, make sure to set the "On Woke up" action to "Broadcast message" in Gadgetbridge.

Also, if it is not set, set the event message to:

```
nodomain.freeyourgadget.gadgetbridge.WokeUp
```

We will let Automate listen for this event message.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/automate_flow_1.jpg){: height="600" style="height: 600px;" }
<div markdown>

Open Automate and create a new flow.

Click the ":material-plus-circle:" icon in the bottom-right to create a new block.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/automate_flow_2.jpg){: height="600" style="height: 600px;" }
<div markdown>

Find and add the ":material-broadcast: Receive broadcast" block to the canvas.

Now, connect "Flow beginning" and "Receive broadcast" together. Then, click on "Receive broadcast" to edit it.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/automate_flow_3.jpg){: height="600" style="height: 600px;" }
<div markdown>

The Action field doesn't let us to write a custom action name, so click on the ":material-function-variant:" icon to switch to the expression mode, and set this value as a action (make sure to wrap in quotes):

```
"nodomain.freeyourgadget.gadgetbridge.WokeUp"
```

It must be the same value as in the Gadgetbridge settings, otherwise it won't work.

Then, click "SAVE" on top-right to save the block.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/automate_flow_4.jpg){: height="600" style="height: 600px;" }
<div markdown>

Now find and add the ":material-bell: Notification show" block to the canvas, and connect it to the previous ":material-broadcast: Receive broadcast" block.

Click on the block that you just added to and set its properties. If you want to add an expression, click on the ":material-function-variant:" icon.

* Proceed: "When dismissed", otherwise the notification will be destroyed before it is shown.
* Title: Set the title of the notification, for example: "Good morning!"
* Message: Set the message of the notification. You can use Automate's built-in functions or you can reference values from other steps. For example, to display the current time in `HH:mm` format:

```
join(concat("You just got woke up in ", dateFormat(Now, "HH:mm"), ", have a nice day!"), "")
```

You can set other configuration options as you wish.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/automate_flow_5.jpg){: height="600" style="height: 600px;" }
<div markdown>

The final flow should look like this.

Finally, go back and start the flow by pressing on blue "Start" button. Now, the flow will wait until it receives the `"nodomain.freeyourgadget.gadgetbridge.WokeUp"` event from Gadgetbridge, which is invoked by Gadgetbridge when you wake up.

Make sure to allow Automate to run in background, otherwise the flow might be stopped by your phone.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/automate_flow_result.jpg){: height="600" style="height: 600px;" }
<div markdown>

If everything is correct, you will receive a notification when you woke up from your next sleep.

The flow will stop when you woke up once, so it won't count if you got asleep and wake up again. As shown in the screenshot, the notification was sent in the middle of the night.

This was just an example, you can play with Automate to set up custom actions for other scenarios, like such as powering on your smart coffee machine when you woke up, or you can check for daytime to prevent running the action in the midnight and so on.

</div></div>

### Download flow file

You can download the flow file that is shown above, so you won't need to build everything from scratch.

[:material-tray-arrow-down: Download .flo file](data:application/octet-stream;base64,TEFGbABjCgPgEAICGfwPBAILnhEGAgIAACAC1AENR29vZCBtb3JuaW5nIfoE4AUD1AEYWW91IGp1c3QgZ290IHdva2UgdXAgaW4g6gSUA9QBBUhIOm1tAADUARIsIGhhdmUgYSBuaWNlIGRheSHUAQAAANQBNGNvbnRlbnQ6Ly9jb20ubGxhbWFsYWIuYXV0b21hdGUucHJvdmlkZXIvaWNvbnMvNjE1MTUAANABP/AAAAAAAADQAQAAAAAAAAAAygE/8AAAAAAAANQBBnN0YXR1c9QBAAAA1AErbm9kb21haW4uZnJlZXlvdXJnYWRnZXQuZ2FkZ2V0YnJpZGdlLldva2VVcAAAAAAAAAAAAAAAAAAAAAADBQ==){: .gb-button download="gadgetbridge-automate-example.flo" }
