---
title: Device actions
---

# Device actions

Some gadgets provide information when a particular event has been detected like when waking up or falling asleep. In Gadgetbridge, you can assign actions to these events.

You can configure Gadgetbridge to either trigger a pre-built action or send a broadcast message. To do this, touch :material-cog: icon under your gadget name to access to "Device specific settings", and go to the "Device actions" _(or similar if your gadget has "different" type of events)._

![](../../assets/static/screenshots/device_actions.jpg){: height="600" style="height: 600px;" }

## Broadcast message

You can pick from one of the built-in actions. There is also a "Broadcast message" action for informing an external app such as [Tasker](https://tasker.joaoapps.com/){: target="_blank" } or [Easer](https://f-droid.org/en/packages/ryey.easer/){: target="_blank" } to let them to perform a custom action.

![](../../assets/static/screenshots/device_actions_broadcast_message.jpg){: height="600" style="height: 600px;" }

<!-- TODO: Add Tasker example -->
