---
title: Topics
icon: material/shape
---

# Features

This category contains information related to Gadgetbridge features.

{{ file_listing() }}
