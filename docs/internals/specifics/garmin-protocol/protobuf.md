---
title: Garmin Protobuf
---

# Garmin Protobuf

This page describes some of the relevant protobuf messages used by Garmin watches.

## GDISmart

This section covers previously undocumented extensions of the `GDISmart` protocol relevant for the Forerunner 245.

Optional fields of the `Smart message`:

| Field number | Message type |
| --- | --- |
| 1 | `CalendarService` |
| 2 | `ConnectIQHTTPService` |
| 3 | `ConnectIQInstalledAppsService` |
| 4 | `ConnectIQAppSettingsService` |
| 7 | `DataTransferService` |
| 8 | `DeviceStatusService` |
| 10 | `AudioPromptsService` |
| 12 | `FindMyWatchService` |
| 13 | `CoreService` |
| 16 | `SmsNotificationService` |
| 21 | `LiveTrackService` |
| 24 | `DeviceMessageService` |
| 27 | `CredentialsService` |
| 30 | `EventSharingService` |
| 42 | `RealtimeSettingsService` |
| 44 | `ToystoreService` |
| 49 | `GNCSService` |

## GNCSService
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `MediaRequest` | `media_request` ||
| 2 | [X] | `MediaResponse` | `media_response` ||

### MediaRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint32` | `notification_uid` ||
| 2 | [X] | `uint32` | `index` ||
| 3 | [X] | `DisplayAttributes` | `attributes` ||
| 4 | repeated | `MediaType` | `supported_types` ||

### MediaResponse
Enums:
```
Status = {UNKNOWN=0, SUCCESS, INVALID_UID, INVALID_INDEX}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `Status` | `status` ||
| 2 | [X] | `uint32` | `notification_uid` ||
| 3 | [X] | `uint32` | `index` ||
| 4 | [X] | `MediaType` | `type` ||
| 5 | [X] | `DataTransferItem` (from `gdi_data_types`) | `transfer_info` ||

### CapabilitiesRequest
This message extends `CoreService.FeatureCapabilitiesRequest` with

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 12 | [X] | `CapabilitiesRequest` | `gncs` ||

and has

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint32` | `np_version` ||
| 2 | [X] | `NotificationDisabledReason` | `notification_disabled_reason` ||

### CapabilitiesResponse
This message extends `CoreService.FeatureCapabilitiesResponse` with

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 12 | [X] | `CapabilitiesResponse` | `gncs` ||

and has

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint32` | `np_version` ||

### DisplayAttributes
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint32` | `width_px` ||
| 2 | [X] | `uint32` | `height_px` ||
| 3 | [X] | `uint32` | `max_byte_size` ||
| 4 | [X] | `uint32` | `quality` ||

## CalendarService
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `CalendarEventsRequest` | `calendar_events_request` ||
| 2 | [X] | `CalendarEventsResponse` | `calendar_events_response` ||

### CalendarEventsRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint64` | `start_date` ||
| 2 | [X] | `uint64` | `end_date` ||
| 3 | [X] | `bool` | `include_organizer` |0|
| 4 | [X] | `bool` | `include_title` |1|
| 5 | [X] | `bool` | `include_location` |1|
| 6 | [X] | `bool` | `include_description` |0|
| 7 | [X] | `bool` | `include_start_date` |1|
| 8 | [X] | `bool` | `include_end_date` |0|
| 9 | [X] | `bool` | `include_all_day` |0|
| 10 | [X] | `uint32` | `max_organizer_length` |0|
| 11 | [X] | `uint32` | `max_title_length` |0|
| 12 | [X] | `uint32` | `max_location_length` |0|
| 13 | [X] | `uint32` | `max_description_length` |0|
| 14 | [X] | `uint32` | `max_events` |100|

### CalendarEventsResponse
Enums:
```
ResponseStatus = {UNKNOWN_RESPONSE_STATUS=0, OK, INVALID_DATE_RANGE}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `ResponseStatus` | `status` ||
| 2 | repeated | `CalendarEvent` | `events` ||

### CalendarEvent
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `string` | `organizer` ||
| 2 | [X] | `string` | `title` ||
| 3 | [X] | `string` | `location` ||
| 4 | [X] | `string` | `description` ||
| 5 | [X] | `uint64` | `start_date` ||
| 6 | [X] | `uint64` | `end_date` ||
| 7 | [X] | `bool` | `all_day` ||
| 8 | repeated | `uint32` | `reminder_time_in_secs` ||

## ConnectIQHTTPService
Optional messages:

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `ConnectIQHTTPRequest` | `connect_iq_http_request` |
| 2 | [X] | `ConnectIQHTTPResponse` | `connect_iq_http_response` |
| 3 | [X] | `ConnectIQImageRequest` | `connect_iq_image_request` |
| 4 | [X] | `ConnectIQImageResponse` | `connect_iq_image_response` |
| 5 | [X] | `RawResourceRequest` | `raw_resource_request` |
| 6 | [X] | `RawResourceResponse` | `raw_resource_response` |
| 7 | [X] | `ConnectIQOAuthRequest` | `connect_iq_oauth_request` |
| 8 | [X] | `ConnectIQOAuthResponse` | `connect_iq_oauth_response` |
| 9 | [X] | `ConnectIQOAuthCompleteRequest` | `connect_iq_oauth_complete_request` |
| 10 | [X] | `ConnectIQOAuthCompleteResponse` | `connect_iq_oauth_complete_response` |
| 11 | [X] | `OpenWebpageNotification` | `open_webpage_notification` |

### ConnectIQHTTPRequest
Enums:
```
HTTPMethod = {UNKNOWN = 0, GET, PUT, POST, DELETE, PATCH, HEAD}
ResponseType = {JSON = 0, URL_ENCODED, PLAIN_TEXT, XML}
Version = {VERSION_1 = 0, VERSION_2}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `url` ||
| 2 | [X] | `HTTPMethod` | `http_method` ||
| 3 | [X] | `bytes` | `http_header_fields` ||
| 4 | [X] | `bytes` | `http_body` ||
| 5 | [X] | `uint32` | `max_response_length` ||
| 6 | [X] | `bool` | `include_http_header_fields_in_response` | 1 |
| 7 | [X] | `bool` | `compress_response_body` | 0 |
| 8 | [X] | `ResponseType` | `response_type` ||
| 9 | [X] | `Version` | `version` | `VERSION_1` |

### ConnectIQHTTPResponse
Enums:
```
ResponseStatus = { UNKNOWN=0, OK=100, INVALID_HTTP_HEADER_FIELDS_IN_REQUEST=200, INVALID_HTTP_BODY_IN_REQUEST=201, INVALID_HTTP_METHOD_IN_REQUEST=202, NETWORK_REQUEST_TIMED_OUT=300, INVALID_HTTP_BODY_IN_NETWORK_RESPONSE=400, INVALID_HTTP_HEADER_FIELDS_IN_NETWORK_RESPONSE=401, NETWORK_RESPONSE_TOO_LARGE=402, INSECURE_REQUEST=1001 }
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `ResponseStatus` | `status` ||
| 2 | [X] | `int32` | `http_status_code` ||
| 3 | [X] | `bytes` | `http_body` ||
| 4 | [X] | `bytes` | `http_header_fields` ||
| 5 | [X] | `uint32` | `inflated_size` ||
| 6 | [X] | `ConnectIQHTTPRequest.ResponseType` | `response_type` ||

### ConnectIQImageRequest
Enums:
```
Dithering = { NONE=0, FLOYD_STEINBERG }
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `url` ||
| 2 | [ ] | `uint32` | `partNumber` ||
| 3 | [X] | `uint32` | `max_width` ||
| 4 | [X] | `uint32` | `max_height` ||
| 5 | [X] | `uint32` | `max_size` ||
| 6 | [X] | `bytes` | `palette` ||
| 7 | [X] | `Dithering` | `dithering` ||
| 8 | [X] | `bool` | `use_data_xfer` ||

### ConnectIQImageResponse
Enums:
```
ResponseStatus = {UNKNOWN=0, OK=100, NETWORK_REQUEST_TIMED_OUT=200, IMAGE_TOO_LARGE=300}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `ResponseStatus` | `status` ||
| 2 | [X] | `int32` | `http_status_code` ||
| 3 | [X] | `bytes` | `image_data` ||
| 4 | [X] | `uint32` | `width` ||
| 5 | [X] | `uint32` | `height` ||
| 6 | [X] | `uint32` | `inflated_size` ||
| 7 | [X] | `DataTransferItem` (from `gdi_data_types`) | `xfer_data` ||

### RawResourceRequest
Enums:
```
HTTPMethod = {UNKNOWN=0, GET, PUT, POST, DELETE, PATCH, HEAD}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `url` ||
| 2 | [X] | `uint32` | `max_size` ||
| 3 | [X] | `HTTPMethod` | `method` | `GET` |
| 4 | [X] | `string` | `body` ||
| 5 | repeated | `HTTPHeader` | `headers` ||
| 6 | [X] | `bool` | `use_data_xfer` ||
| 7 | [X] | `bytes` | `raw_body` ||
| 8 | repeated | `MultipartForm` | `forms` ||
| 9 | [X] | `int32` | `connection_timeout_seconds` | 60 |
| 10 | [X] | `int32` | `read_timeout_seconds` | 60 |


#### HTTPHeader
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `key` ||
| 2 | [ ] | `string` | `value` ||

#### MultipartForm
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `string` | `name` ||
| 2 | [X] | `string` | `filename` ||
| 3 | [X] | `string` | `content_type` ||
| 4 | [X] | `bytes` | `resource_data` ||
| 5 | [X] | `DataTransferItem` | `xfer_data` ||

### RawResourceResponse
Enums:
```
ResponseStatus = { UNKNOWN=0, OK=100, NETWORK_REQUEST_TIMED_OUT=200, FILE_TOO_LARGE=300, DATA_TRANSFER_ITEM_FAILURE=400 }
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `ResponseStatus` | `status` ||
| 2 | [X] | `int32` | `http_status_code` ||
| 3 | [X] | `bytes` | `resource_data` ||
| 4 | [X] | `DataTransferItem` | `xfer_data` ||
| 5 | repeated | `HTTPHeader` | `headers` ||

#### HTTPHeader
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `key` ||
| 2 | [ ] | `string` | `value` ||

### ConnectIQOAuthRequest
Enums:

```
ResultFormat = {URL=0, BODY_JSON=1}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `initial_url` ||
| 2 | [X] | `bytes` | `initial_url_parameters` ||
| 3 | [X] | `bool` | `parameters_encrypted` ||
| 4 | [X] | `bool` | `parameters_compressed` ||
| 5 | [ ] | `string` | `result_url` ||
| 6 | [X] | `ResultFormat` | `result_type` ||
| 7 | [X] | `bytes` | `result_keys` ||
| 8 | [X] | `string` | `app_name` ||
| 9 | [ ] | `bytes` | `app_uuid` ||
| 10 | [X] | `bytes` | `store_uuid` ||

### ConnectIQOAuthResponse
Enums:

```
ResponseStatus = {UNKNOWN=0, OK=1, BAD_REQUEST=2, NOTIFICATION_FAILURE=3}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `ResponseStatus` | `status` ||
| 2 | [X] | `int32` | `http_status_code` ||
| 3 | [ ] | `bytes` | `app_uuid` ||
| 4 | [X] | `bytes` | `data` ||
| 5 | [X] | `bool` | `encrypted` ||
| 6 | [X] | `bool` | `compressed` ||

### ConnectIQOAuthCompleteResponse
Enums:

```
ResponseStatus = {UNKNOWN=0, OK=1}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `ResponseStatus` | `status` ||

### OpenWebpageNotification
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `url` ||
| 2 | [X] | `bytes` | `params` ||
| 3 | [X] | `string` | `app_name` ||

## DataTransferService
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `DataDownloadRequest` | `data_download_request` ||
| 2 | [X] | `DataDownloadResponse` | `data_download_response` ||
| 3 | [X] | `InitiateDataUploadRequest` | `initiate_data_upload_request` ||
| 4 | [X] | `InitiateDataUploadResponse` | `initiate_data_upload_response` ||
| 5 | [X] | `DataUploadRequest` | `data_upload_request` ||
| 6 | [X] | `DataUploadResponse` | `data_upload_response` ||
| 7 | [X] | `DataUploadCanceledNotification` | `data_upload_canceled_notification` ||
| 8 | [X] | `FileAccessItemRequest` | `file_access_item_request` ||
| 9 | [X] | `FileAccessItemResponse` | `file_access_item_response` ||

### CapabilitiesRequest
This message extends `CoreService.FeatureCapabilitiesRequest` with

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 14 | [X] | `CapabilitiesRequest` | `data_xfer_request` ||

and has

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint32` | `service_version` ||

### CapabilitiesResponse
This message extends `CoreService.FeatureCapabilitiesResponse` with

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 14 | [X] | `CapabilitiesResponse` | `data_xfer_response` ||

### DataDownloadRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint32` | `id` ||
| 2 | [ ] | `uint32` | `offset` ||
| 3 | [X] | `uint32` | `max_chunk_size` | 4096 |

### DataDownloadResponse
Enums:
```
Status = {UNKNOWN=0, SUCCESS, INVALID_ID, INVALID_OFFSET}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `Status` | `status` ||
| 2 | [ ] | `uint32` | `id` ||
| 3 | [ ] | `uint32` | `offset` ||
| 4 | [X] | `bytes` | `payload` ||

### InitiateDataUploadRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint32` | `id` ||

### InitiateDataUploadResponse
Enums:
```
Status = {UNKNOWN=0, OK, INVALID_ID}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint32` | `id` ||
| 2 | [ ] | `Status` | `status` ||

### DataUploadRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint32` | `id` ||
| 2 | [ ] | `uint32` | `offset` ||
| 3 | [ ] | `bytes` | `payload` ||

### DataUploadResponse
Enums:
```
Status = {UNKNOWN=0, SUCCESS, INVALID_ID, INVALID_OFFSET, SIZE_MISMATCH, ABORT}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint32` | `id` ||
| 2 | [ ] | `Status` | `status` ||

### DataUploadCanceledNotification
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint32` | `id` ||

### FileAccessItemRequest
Enums:
```
Latency = { STANDARD=0, SOON, IMMEDIATE, DURING_ACTIVITY }
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint32` | `data_xfer_id` ||
| 2 | [X] | `Latency` | `expected_latency` |`STANDARD`|

### FileAccessItemResponse
Enums:
```
Status = { UNKNOWN=0, OK, INVALID_ID }
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [x] | `status` | `status` ||
| 2 | [x] | `uint32` | `data_xfer_id` ||
| 3 | [x] | `uuid` | `file_access_item_uid` ||

## ConnectIQInstalledAppsService
Enums relevant for submessages:

```
AppType = {UNKNOWN_APP_TYPE=0, WATCH_APP=1, WIDGET=2, WATCH_FACE=3, DATA_FIELD=4, ALL=5, NONE=6, AUDIO_CONTENT_PROVIDER=7, ACTIVITY=8}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `GetInstalledAppsRequest` | `get_installed_apps_request` ||
| 2 | [X] | `GetInstalledAppsResponse` | `get_installed_apps_response` ||
| 3 | [X] | `DeleteAppRequest` | `delete_app_request` ||
| 4 | [X] | `DeleteAppResponse` | `delete_app_response` ||
| 5 | [X] | `UpdateInstalledAppsRequest` | `update_installed_app_request` ||
| 6 | [X] | `UpdateInstalledAppsResponse` | `update_installed_app_response` ||
| 7 | [X] | `EnableNativeAppRequest` | `enable_native_app_request` ||
| 8 | [X] | `EnableNativeAppResponse` | `enable_native_app_response` ||
| 9 | [X] | `LaunchAppRequest` | `launch_app_request` ||
| 10 | [X] | `LaunchAppResponse` | `launch_app_response` ||

### GetInstalledAppsRequest

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `AppType` | `app_type` ||

### GetInstalledAppsResponse

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint64` | `available_space` ||
| 2 | [ ] | `uint32` | `available_slots` ||
| 3 | repeated | `InstalledApp` | `installed_apps` ||

#### InstalledApp
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `store_app_id` ||
| 2 | [ ] | `AppType` | `app_type` ||
| 3 | [ ] | `string` | `name` ||
| 4 | [ ] | `bool` | `disabled` ||
| 5 | [X] | `uint32` | `version` ||
| 6 | [X] | `string` | `filename` ||
| 7 | [X] | `uint64` | `filesize` ||
| 8 | [X] | `uint32` | `native_app_id` ||
| 9 | [X] | `bool` | `favorite` ||

### DeleteAppRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `store_app_id` ||
| 2 | [ ] | `AppType` | `app_type` ||

### DeleteAppResponse
Enums:

```
Status = {UNKNOWN_STATUS=0, OK=1, FAILED_TO_DELETE=2}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `Status` | `status` ||

### UpdateInstalledAppsRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | repeated | `InstalledApp` | `installed_app` ||

#### InstalledApp
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_id` ||
| 2 | [ ] | `AppType` | `app_type` ||
| 3 | [ ] | `bool` | `disabled` | `false` |
| 4 | [X] | `bool` | `favorite` ||

### UpdateInstalledAppsResponse
Enums:

```
Status = {UNKNOWN_STATUS=0, OK=1, FAILED_TO_UPDATE=2}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `Status` | `status` ||

### EnableNativeAppRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint32` | `native_app_id` ||
| 2 | [ ] | `AppType` | `app_type` ||
| 3 | [x] | `bytes` | `app_id` ||

### EnableNativeAppResponse
Enums:

```
Status = {UNKNOWN_STATUS=0, OK=1, INVALID_NATIVE_APP=2}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `Status` | `status` ||

### LaunchAppRequest
Enums:

```
StartUpMode = {APP_STARTUP_MODE_NORMAL=0, APP_STARTUP_MODE_MEDIA_PLAYBACK_CONFIGURATION=1, APP_STARTUP_MODE_MEDIA_SYNC_CONFIGURATION=2, APP_STARTUP_MODE_NO_UI=3}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_uuid` ||
| 2 | [ ] | `StartUpMode` | `app_startup_mode` ||

### LaunchAppResponse
Enums:

```
Status = {LAUNCH_STATUS_SUCCESS=0, LAUNCH_STATUS_APP_ALREADY_RUNNING=1, LAUNCH_STATUS_APP_NOT_INSTALLED=2, LAUNCH_STATUS_UNKNOWN_ERROR=3}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `Status` | `status` ||

## ConnectIQAppSettingsService
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `GetAppSettingsRequest` | `get_app_settings_request` ||
| 2 | [X] | `GetAppSettingsResponse` | `get_app_settings_response` ||
| 3 | [X] | `SaveAppSettingsRequest` | `save_app_settings_request` ||
| 4 | [X] | `SaveAppSettingsResponse` | `save_app_settings_response` ||
| 5 | [X] | `SendAppSettingsRequest` | `send_app_settings_request` ||
| 6 | [X] | `SendAppSettingsResponse` | `send_app_settings_response` ||
| 7 | [X] | `OpenAppSettingsRequest` | `open_app_settings_request` ||
| 8 | [X] | `OpenAppSettingsResponse` | `open_app_settings_response` ||
| 9 | [X] | `VerifyAppSettingsRequest` | `verify_app_settings_request` ||
| 10 | [X] | `VerifyAppSettingsResponse` | `verify_app_settings_response` ||

### GetAppSettingsRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_identifier` ||

### GetAppSettingsResponse
Enums:

```
Response = {UNKNOWN_RESPONSE=0, SUCCESS=1, APP_NOT_INSTALLED=2}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_identifier` ||
| 2 | [X] | `Response` | `response` ||

### SendAppSettingsRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_identifier` ||
| 1 | [X] | `bytes` | `settings_value` ||

### SendAppSettingsResponse
Enums:

```
Response = {UNKNOWN_RESPONSE=0, SUCCESS=1}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `Response` | `response` ||

### SaveAppSettingsRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_identifier` ||
| 1 | [X] | `bytes` | `settings_value` ||

### SaveAppSettingsResponse
Enums:

```
Response = {UNKNOWN_RESPONSE=0, SUCCESS=1, FAILED_TO_SAVE=2}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_identifier` ||
| 2 | [X] | `Response` | `response` ||

### OpenAppSettingsRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `store_uuid` ||

### OpenAppSettingsResponse
Enums:

```
ResponseStatus = {UNKNOWN_RESPONSE=0, SUCCESS=1}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `app_identifier` ||
| 2 | [ ] | `ResponseStatus` | `status` ||

### VerifyAppSettingsRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `store_uuid` ||
| 2 | [ ] | `bytes` | `key` ||
| 3 | [X] | `bytes` | `value` ||

### VerifyAppSettingsResponse
Enums:

```
ResponseStatus = {UNKNOWN=0, OK=1, APP_NOT_INSTALLED=2, APP_CANNOT_RUN=3}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `store_uuid` ||
| 2 | [X] | `ResponseStatus` | `status` ||

### VerifiedAppSettingsNotification
Enums:

```
Result = {VERIFIED=0, UNKNOWN_ERROR=1, APP_ERROR=2, VERIFICATION_FAILED=3}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `store_uuid` ||
| 2 | [X] | `bytes` | `key` ||
| 3 | [X] | `Result` | `result` ||
| 4 | [X] | `bytes` | `error` ||

## DeviceMessageService
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `CIQMessageSubscribeNotification` | `message_subscribe` ||
| 2 | [X] | `CIQMessageUnsubscribeNotification` | `message_unsubscribe` ||
| 3 | [X] | `CIQMessageRequest` | `message_request` ||
| 4 | [X] | `CIQMessageResponse` | `message_response` ||

### CIQMessageSubscribeNotification
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `string` | `last_event_id` ||

### CIQMessageUnsubscribeNotification
This message is empty.

### CIQMessageRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `string` | `event_id` ||
| 2 | [X] | `string` | `app_id` ||
| 3 | [X] | `string` | `payload` ||

### CIQMessageResponse
Enums:

```
Status = {SUCCESS=0, FAILURE=1}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `string` | `event_id` ||
| 2 | [X] | `Status` | `status` ||

## GDIToystore
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `ConnectIQRunCodeRequest` | `run_code_request` ||
| 2 | [X] | `ConnectIQRunCodeResponse` | `run_code_response` ||
| 3 | [X] | `ConnectIQHTTP.ConnectIQOAuthRequest` | `connect_iq_oauth_request` ||
| 4 | [X] | `ConnectIQHTTP.ConnectIQOAuthResponse` | `connect_iq_oauth_response` ||
| 5 | [X] | `ConnectIQHTTP.ConnectIQOAuthCompleteRequest` | `connect_iq_oauth_complete_request` ||
| 6 | [X] | `ConnectIQHTTP.ConnectIQOAuthCompleteResponse` | `connect_iq_oauth_complete_response` ||
| 7 | [X] | `ConnectIQPurchaseRequest` | `purchase_request` ||
| 8 | [X] | `ConnectIQPurchaseResponse` | `purchase_response` ||

### ConnectIQRunCodeRequest
Enums:

```
RunType = {AUTHENTICATION=0}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `bytes` | `app_uuid` ||
| 2 | [X] | `RunType` | `run_type` ||

### ConnectIQRunCodeResponse
Enums:

```
ResponseStatus = {OK=0, UNKNOWN_ERROR=1, APP_NOT_INSTALLED=2, NOT_ENOUGH_MEMORY=3}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `ResponseStatus` | `status` ||

### ConnectIQPurchaseRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `bytes` | `manifest_uuid` ||
| 2 | [ ] | `bytes` | `store_uuid` ||
| 3 | [X] | `string` | `app_name` ||
| 4 | [X] | `string` | `promo_code` ||

### ConnectIQPurchaseResponse
Enums:

```
Status = {OK=0, NOT_HANDLED=1}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `Status` | `status` ||

### CapabilitiesRequest
This message extends `CoreService.FeatureCapabilitiesRequest` with

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 21 | [X] | `CapabilitiesRequest` | `toy_store_request` ||

### CapabilitiesResponse
This message extends `CoreService.FeatureCapabilitiesResponse` with

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 21 | [X] | `CapabilitiesResponse` | `toy_store_response` ||

and has

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `bool` | `app_purchase_support` ||

## LiveTrackMessagingService
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `SendLiveTrackMessageRequest` | `send_live_track_message_request` ||
| 2 | [X] | `SendLiveTrackMessageResponse` | `send_live_track_message_response` ||
| 3 | [X] | `GetLiveTrackMessagesRequest` | `get_live_track_messages_request` ||
| 4 | [X] | `GetLiveTrackMessagesResponse` | `get_live_track_messages_response` ||

### SendLiveTrackMessageRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | repeated | `string` | `recipient_session_ids` ||
| 2 | [ ] | `LiveTrackMessageBody` | `live_track_message_body` ||

### SendLiveTrackMessageResponse
Enums:

```
SendLiveTrackMessageStatus = {OK=0, UNKNOWN_ERROR=1, NETWORK_FAILURE=2, TIMEOUT=3, SERVER_ERROR=4, INVALID_SERVER_RESPONSE=5, NOT_LIVE_TRACKING=6, EMPTY_SESSION_IDS=7}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `SendLiveTrackMessageStatus` | `send_live_track_message_status` ||
| 2 | repeated | `SendLiveTrackMessageError` | `send_live_track_message_errors` ||

### SendLiveTrackMessageError
Enums:

```
LiveTrackMessageErrorType = {UNKNOWN_ERROR=0, SENDER_SESSION_INVALID=1, SENDER_SESSION_ENDED=2, RECIPIENT_SESSION_INVALID=3, RECIPIENT_SESSION_ENDED=4}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | repeated | `string` | `session_ids` ||
| 2 | [ ] | `LiveTrackMessageErrorType` | `live_track_message_error_type` ||
| 3 | [X] | `string` | `error_message_text` ||

### GetLiveTrackMessagesRequest
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [X] | `uint64` | `last_received_tracker_message_seq` | `0` |

### GetLiveTrackMessagesResponse
Enums:

```
GetLiveTrackMessagesStatus = {OK=0, UNKNOWN_ERROR=1, NETWORK_FAILURE=2, TIMEOUT=3, SERVER_ERROR=4, INVALID_SERVER_RESPONSE=5, NOT_LIVE_TRACKING=6}
```

| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `GetLiveTrackMessagesStatus` | `get_live_track_messages_status` ||
| 2 | repeated | `LiveTrackMessage` | `live_track_messages` ||
| 3 | [X] | `uint32` | `call_interval` ||

### LiveTrackMessage
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `string` | `sender_session_id` ||
| 2 | [ ] | `string` | `sender_display_name` ||
| 3 | [ ] | `uint64` | `tracker_message_seq` ||
| 4 | [ ] | `LiveTrackMessageBody` | `live_track_message_body` ||

### LiveTrackMessageBody
| Field number | Optional | Message type | Field name | Default |
| --- | --- | --- | --- | --- |
| 1 | [ ] | `uint64` | `message_id` ||
| 2 | [ ] | `string` | `message_text` ||
| 3 | [ ] | `uint64` | `timestamp` ||
