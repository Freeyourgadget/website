---
title: Galaxy Buds Protocol
---

# Galaxy Buds Protocol

The protocol is partially described in [this document here](https://github.com/ThePBone/GalaxyBudsClient/blob/master/GalaxyBudsRFCommProtocol.md){: target="_blank" } in the [GalaxyBudsClient](https://github.com/ThePBone/GalaxyBudsClient){: target="_blank" } project.

The battery values are retrieved based on this [python script](https://github.com/ThePBone/GalaxyBuds-BatteryLevel/blob/master/buds_battery.py){: target="_blank" }. There is even a [Gnome Shell extension](https://github.com/sidilabs/galaxybuds-gnome-extension){: target="_blank" } for the buds and it uses the same python script, adding here just for completeness. The most important find [is this issue](https://github.com/ThePBone/GalaxyBudsClient/issues/8){: target="_blank" }, which describes why the methods used to retrieve the battery levels in the python script are different then those used in the protocol description. The [GalaxyBudsClient](https://github.com/ThePBone/GalaxyBudsClient){: target="_blank" } uses a special debug option to retrieve more data, thus the `message_id`s are different then the normal way of doing this. This is not important in Gadgetbridge as we only retrieve the battery values.

RFComm message [can be found here.](https://github.com/ThePBone/GalaxyBudsClient/blob/master/GalaxyBudsClient/Message/SPPMessage.cs){: target="_blank" }

Command IDs [can be found here.](https://github.com/ThePBone/GalaxyBudsClient/blob/master/GalaxyBudsClient/Message/SPPMessagePrivate.cs){: target="_blank" }
