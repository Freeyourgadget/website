---
title: Amazfit Bip U Firmware Update
---

# Amazfit Bip U Firmware Update

--8<-- "blocks.md:firmware_danger"

## Getting the firmware

Since we may not distribute the firmware, you have to find it elsewhere.

## Installing the firmware

Flashing the firmware is supported since Gadgetbridge 0.61.0

First install the `.fw`, then `.res`, and finally `.ft`.

Please note that the Bip U will tell you after the update of the `.fw` and the reboot that the update failed. This is not true, it just needs the correct `.res` and `.ft` flashed afterwards. The message will disappear then.

## Known firmware versions

### Bip U

fw ver   | tested | notes                   | res ver |fw-md5 | res-md5 | 
---------|--------|-------------------------|---------|-------|---------|
1.0.1.76 | yes    | pre-installed firmware  | ?       |   ?   | ?       |
1.0.2.95 | yes    |                         | 36      | cf7766dab4b473d5336685545aebbfb9 | db30e9a4d06044b52b3b583967d2dd59 |

### Bip U Pro

fw ver   | tested | notes                   | res ver |fw-md5 | res-md5 | 
---------|--------|-------------------------|---------|-------|---------|
1.0.2.28 | yes    | pre-installed firmware  | ?       |   ?   | ?       |
1.0.2.92 | yes    |                         | 36      | ce65b04f710f53d7435e61cfbc1b10af | 0e6a700cd42885615b9b2fc18c398139 |
