---
title: FitPro Protocol
---

# FitPro Protocol

```
cd 00 2a 05 01 0c 00 0c 0000012d000000bb000018
|  |  |  |  |  |  |  |  |-- payload ->
|  |  |  |  |  |  |  |----- payload length low
|  |  |  |  |  |  |-------- payload length high
|  |  |  |  |  |----------- command
|  |  |  |  |-------------- delimiter/version
|  |  |  |----------------- command group
|  |  |-------------------- full length low
|  |----------------------- full length high
|-------------------------- header
```

The protocol seems to be a variation on a frequently used style of communication as seen in different bands inside Gadgetbridge.

* Wireshark dissector and a colorscheme for the FitPro communication protocol, available in the [gadgetbridge-tools repo](https://codeberg.org/Freeyourgadget/Gadgetbridge-tools/src/branch/main/fitpro){: target="bridge" }.
* Frida script to list classes and get console log (could also be retrieved simply via adb) of 
the FitPro app, available in the [gadgetbridge-tools repo](https://codeberg.org/Freeyourgadget/Gadgetbridge-tools/src/branch/main/fitpro){: target="bridge" }.
* There is also [this project](https://github.com/vanous/MFitX/blob/AntiGoogle/app/src/main/java/anonymouls/dev/mgcex/app/backend/LM517CommandInterpreter.kt){: target="bridge" } which seem to already fetch some stuff and do other things.

## MTU

Some bands do require MTU size of 20. Other bands do not require it but work OK with this value.
