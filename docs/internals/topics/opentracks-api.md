---
title: OpenTracks API
---

# OpenTracks API

## Introduction

This is a programming interface based on Android Intents to communicate with [OpenTracks](https://opentracksapp.com/){: target="_blank" } - a FLOSS sports tracking app. OpenTracks can record GPS based activity/workouts and while doing that, provide live update of current status, for example speed, duration of the activity and so on.

## For developers

Smart device developers of Gadgetbridge can use this programming interface to start GPS tracking done by external app (OpenTracks) for a smart band/watch device without GPS and without support for externally proviced GPS data. Gadgetbridge has an internal implementation which smart devices can use, currently the best implementation in Gadgetbridge is for the Fossil smart watch where it is used for it's workout app. Gadgetbridge also allows Mibands/Amazfit bands/watches to use this API as a remote trigger via [Button and Device actions](../automations/events.md). The Button actions allow you can to start activity recording in OpenTracks by using the button on the smart band/watch. In the Debug menu in Gadgetbridge, there are Start/Stop/Status buttons to do the same without any involvement of a on wrist device.

The Public API is disabled by default to protect the user's privacy, but it can easily be enabled in the settings.

## What does it do for the user

This can remotely start (and i the future also stop) OpenTracks fitness activity recording. OpenTracks can provide feedback about current status (duration of activity, speed and so on) of the activity. This is very similar to the "Workout recording" in MiFit does (with the selection of type of workout and so on...). See short step by step manual below for how to use this.

## How to use this practically

- Install OpenTracks [Nightly release](https://fdroid.storchp.de/fdroid/repo/){: target="_blank" } or [official release on F-Droid](https://f-droid.org/packages/de.dennisguse.opentracks/){: target="_blank" }.
- Enable the API in OpenTracks
- In Gadgetbridge settings, select "OpenTracks package name" based on which OpenTracks version you installed. (official, playstore, debug, nightly...)
- Use this function either in:
    - Fossil: 
        - it is implemented as a workout app - when a workout is started, the OpenTrack is started to record the activity.
    - In Miband/Amazfit devices which support [Button and Device actions](../automations/events.md):
        - set "Fitness app tracking start/stop/toggle" as one of the Button/Device actions
    - Gadgetbridge Debug menu:
        - use the "Fitness app tracking start/stop" and "Show Fitness App Tracking status"
- One could also use this onn Miband/Amazfit devices which support [Button and Device actions](../automations/events.md) and send an Android Broadcast Intent. For more details on these intents, see [OpenTracks description](https://github.com/OpenTracksApp/OpenTracks/#public-api){: target="_blank" }.

## API Implementation in Gadgetbridge

[View the source code here](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/externalevents/opentracks/){: target="_blank" }

## A short explanation of how this integration works

Starting the recording from a device requires calling `startRecording()` on this class. For a simple example, check out the implementation in `WorkoutRequestHandler`, used by the Fossil HR series. The OpenTracks class can be set in the Gadgetbridge settings and depends on the installation source used for OpenTracks. Details can be found in their documentation here: https://github.com/OpenTracksApp/OpenTracks#api `startRecording()` sends an explicit Intent to OpenTracks signalling it to start recording. It passes along the package name and class name of our `OpenTracksController` which OpenTracks will use to send the statistics URIs to. After starting the recording service, OpenTracks uses a new explicit Intent to start our `OpenTracksController` and passes along the URIs and the read permissions for those URIs (using `Intent.FLAG_GRANT_READ_URI_PERMISSION`). So at that point `OpenTracksController` is started as a new `Activity` (or `Context`) which has the read permissions for the statistics URIs. The controller saves its `Context` into the `OpenTracksContentObserver` in the GB main process, so it can keep running and read the statistics with the correct `Context`. So, whatever class, device or activity calls the methods on the `OpenTracksContentObserver` from, it will always work.

Here are suppoorted Android Intents which OpenTracks can receive:

* **Start a recording:** `de.dennisguse.opentracks.publicapi.StartRecording`
* **Stop a recording:** `de.dennisguse.opentracks.publicapi.StopRecording`

`StartRecording` supports the following parameters:

* Set track data: `TRACK_NAME`, `TRACK_CATEGORY`, and `TRACK_DESCRIPTION`

## Testing the OpenTracks API from adb

Start: 

```
adb shell am start -n de.dennisguse.opentracks.debug/de.dennisguse.opentracks.publicapi.StartRecording
```


Stop: 

```
adb shell am start -n de.dennisguse.opentracks.debug/de.dennisguse.opentracks.publicapi.StopRecording
```

