---
title: Huami GPS
---

# Huami GPS

Some gadgets (Bip, Bip S...) contain a GPS receiver.

The GPS chip performs better if it has recent aGPS data.

aGPS data can be uploaded to some devices using the instructions below. Not all newer devices can be updated with aGPS data via Gadgetbridge, per {{ 2253|issue }}

This can be obtained via [huami-token](https://codeberg.org/argrento/huami-token){: target="_blank" }, for example:

```
python ./huami_token.py -m xiaomi -e my@email -p p4ssword -g
```

That will download the following two files:

* `cep_7days.zip`
* `cep_alm_pak.zip`

From the cep_alm_pak.zip you unzip `cep_pak.bin` and `gps_alm.bin` and flash into the watch.

You can also do the same with the huami-token python script wrapped into python GUI kivi and packaged for Android as the [huafetcher](https://codeberg.org/vanous/huafetcher){: target="_blank" }.

It downloads and unzips the `cep_pak.bin` and `gps_alm.bin` into `/storage/emulated/0`.

## Zepp OS

Zepp OS devices can update aGPS data by installing an EPO zip file. This is currently enabled for the some gadgets.

The changes to fetch EPO files on `huami-token` and `huafetcher` are not yet merged:

* <https://codeberg.org/argrento/huami-token/pulls/79>{: target="_blank" }
* <https://codeberg.org/vanous/huafetcher/pulls/2>{: target="_blank" }
