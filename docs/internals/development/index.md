---
title: Development
icon: material/code-tags
---

# Development

This category contains information related to the development of Gadgetbridge.

{{ file_listing() }}
