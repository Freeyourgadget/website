---
title: What is the best device?
---

# What is the best device?

This is very difficult to answer and it depends on your needs and style, for example whether you want the device for sports, for development, for notifications and so on. Look at [Gadgets](../gadgets/index.md) page for more details. We would also appreciate if you help to fill the documentation pages of devices with more useful info after you have used the device for some time.

### Pebble {: #pebble }

Pebble used to be the main driving force for features in Gadgetbridge and it had a lot of functions, including writing own apps for the watch. The step counting was not great, but notification and things like SMS replies from the watch were nice.

### Amazfit / Mi Bands {: #huami }

Mi Band / Amazfit gadgets have gained a lot of popularity and have good support in Gadgetbridge. They have fairly accurate step and heart rate sensors. Total sleep hours are measured and info is provided but sleep cycles (light/deep sleep) are not accurately discovered, but the heart rate pattern allows you to observe your sleep patterns very well. Support in Gadgetbridge includes workout tracking and also workouts that require GPS.

The new Amazfit / Mi Band gadgets now require you to first obtain a secret key from the official app before you can pair them with Gadgetbridge.

Amazfit Bip watch and the Mi Band 3 band were the last gadgets that did not require obtaining of this secret paring code for pairing.

Mi Band 5 requires obtaining of the secret pairing key but the communication between the band and the watch is not encrypted and thus this gadget is nice for reverse engineering.

Mi Band 6 is a really nice band with good font support, full front-face screen display and great features. The communication is even more locked down: to pair you must first get the secret key and it is not possible to just observe the Bluetooth traffic for reverse engineering because the data is encrypted.

Zepp OS watches are fairly well supported, but like many other devices require an auth key by using the official app at least once. The list if supported features is similar to other Huami devices. Zepp OS supports writing custom apps and for a tinkerer, this can open a lot of possibilities, even though the app support for Zepp in Gadgetbridge is currently limited. Device support includes [loyalty card support by Catima app.](../basics/integrations/catima.md)

Other Xiaomi watches such as the Mi Band 8 and Redmi watches are also starting to be supported, but are currently in an experimental state. They also require an auth key from the official app.

### Bangle.js {: #bangle }

Bangle.js, mainly in its version 2 is quite popular and has good features, ongoing development, own apps and is completely open, but it may not be as polished as the commercial products. It allows own apps and sending of custom data via Intents.

### PineTime {: #pinetime }

Same can be said about the PineTime which is also an open source smart watch where firmware is a work in progress with features being gradually added in.

### Fossil/Skagen {: #fossil }

Fossil Hybrid HR is another very popular watch with a lot of features, possibility to create own apps, send custom data via Intents. Sports tracking features are not bad but are not great, there is no sleep tracking (in Gadgetbridge). It requires you to update it with the official app at least once and also to get a secret key before you can pair it with Gadgetbridge.

Owners often chose these watches because of their stylish looks and battery life (2 weeks for "gen 1" and 5 weeks for "gen 6").

The Skagen Jorn and Citizen CZ Smart series are supported by Gadgetbridge as well, as they contain the same internals as the Fossil watches.

### Huawei/Honor

[Huawei/Honor](../gadgets/wearables/huawei-honor.md) watches and bands have also seen recent support by Gadgetbridge, as of version 0.78.0. They are mostly supported, with some missing features and known issues. Unlike a lot of devices, they do not require the official app at all to be used with Gadgetbridge.

### Other devices? {: #other }

* Many are supported, please help to add details to our documentation.
* [Garmin devices?](../gadgets/wearables/garmin.md) Initial support is in, help is wanted.
