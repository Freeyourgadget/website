---
title: Helping with translations
---

# Helping with translations

The way to contribute translations is by using [Weblate](https://hosted.weblate.org/engage/freeyourgadget/){: target="_blank" }.

To start translating, just:

* Create a Weblate account
* Start translating :-)

Weblate is free software, just like Gadgetbridge, hence it is a perfect fit for us.

The translations are usually incorporated shortly before a release.
