---
title: When will a new release appear?
---

# When will a new release appear?

Unfortunately we have no control on when a new release will appear on F-Droid, but there are a few steps you can perform to check by yourself and follow the progress.

The last step we perform to mark the codebase as ready to be released by F-Droid, is to add a _tag_ to our codebase. This tag is an information for the F-Droid build-bot that a new version is ready, and must be "picked up" by the build bot. This happens automatically, usually within a few hours, but **we have no control over this part of the process**.

## How to check if F-Droid picked up a new release?

Open the [metadata page](https://gitlab.com/fdroid/fdroiddata/blob/master/metadata/nodomain.freeyourgadget.gadgetbridge.yml){: target="_blank" } on F-Droid's Gitlab repository and check if the new tag was added at the end of the file. The number that follows the comma on each ``Build:`` line is the build number, that you will need in the next step.

After the new release has been "picked up", the actual application must be built by F-Droid. This happens automatically, usually within a few days, but **we have no control over this part of the process**.

## How to check if F-Droid built the release?

[See here](https://f-droid.org/wiki/index.php?title=nodomain.freeyourgadget.gadgetbridge/lastbuild){: target="_blank" } to know when was last built being process and whether it succeeded or failed, listed at the bottom of the page.

After the new release has been built, the resulting APK must be signed by F-Droid, the index of the main repository must be rebuilt and the new APK made available. This happens automatically, usually within a few days, but **we have no control over this part of the process**.

## How to check if F-Droid released to the index?

If an `.apk` was made available, this implies its being signed and the index updated, so we only need to check for the `.apk` here. For that, we have several options:

* Perform a manual refresh of the repo index(es) in your F-Droid client. If you have Gadgetbridge installed, you should be notified about an available update. If not, you still can search for the app and open its detail page, available builds should be listed.
* Visit the [Gadgetbridge web page on the F-Droid repo](https://f-droid.org/packages/nodomain.freeyourgadget.gadgetbridge/){: target="_blank" }. In the "Packages" section, just below the blue "Download F-Droid" button, available packages are shown. What's listed here should reflect the latest index build. Note that the webpage is updated with a delay, so it doesn't always reflect the latest changes.
* Download the `index.xml` (or `index-v1.json`) manually and parse it (the harder way).

If you want to find out whether F-Droid has published any new apps or updates within a given time frame (aka "did they build anything at all lately?"), check with the [IzzyOnDroid Repo Browser](https://apt.izzysoft.de/fdroid/index.php?repo=main){: target="_blank" }. Here you can filter by "added since" (new updates) and/or "updated since" (updates to already existing apps) to get an idea on "successful activity" in this context (the repo indexes at IzzyOnDroid are updated once per day, so they might not always reflect latest activities while more often than not being updated faster than the official website). Not only for the F-Droid main repo, by the way :wink:
