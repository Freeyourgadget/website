---
title: Basics
icon: material/palette-swatch-variant
---

# Basics

The documentation has been separated into two sections: "Basics" is for end-user information and "Internals" for technical topics. This section of the documentation contains useful information about getting started with Gadgetbridge and about how to pair and use the app.

## Quick links

<div class="gb-section" markdown>
:material-watch:
<div markdown>

Gadgetbridge supports many wearables from different brands, but it is not limited to just wearable gadgets. To see a list of all gadgets that are supported in Gadgetbridge, see [Gadgets.](../gadgets/index.md)

</div></div>

<div class="gb-section" markdown>
:material-list-status:
<div markdown>

When Gadgetbridge is first launched, it will request a lot of permissions, this is due to the amount of supported features that require permissions, like notifications, call logs, phone state, calendar, and so on. See [List of permissions](./topics/permissions.md) for more details.

</div></div>

---

## Get started

If you haven't already, install Gadgetbridge from our home page. When you're ready, jump to the "Pairing" section with the button below to pair your first gadget.

[:material-arrow-right: Pair your first gadget](./pairing/index.md){: .gb-button }
