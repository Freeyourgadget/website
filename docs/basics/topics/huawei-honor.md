---
title: Huawei/Honor gadgets
---

# Huawei/Honor gadgets

List of supported Huawei and Honor gadgets:

{% for key, device in device_support.items() | sort %}
    {%- if device.vendor == "huawei" or device.vendor == "honor" %}
        {% set name = key.replace("_", " ").title().replace(" Gt", " GT").replace(" Aw", " AW") %}
        {% set icon = ' :material-flask-empty-outline:{: title="Experimental" }' if "experimental" in device.flags else "" %}
* [{{ name }}](../../gadgets/wearables/huawei-honor.md#device__{{ key }}) {{ icon }}
    {% endif -%}
{% endfor %}


## Supported features

Support for Huawei and Honor devices was added in {{ 2462 | pull }}.

Note that not all devices have support for all features.

* ??? sublist "Set band settings"
    * Time
    * Language
    * Do not disturb
    * Inactivity warnings
    * Rotate wrist to switch info (only on some devices)
    * Activate display on wrist lift
    * Notification on bluetooth disconnect
    * User information (height, weight, age, year of birth, gender)
    * Enable measuring skin temperature (only on some devices, sync not yet implemented)
* ??? sublist "Alarms"
    * Including smart alarm (only on some devices)
    * Retrieving alarms that were changed on the device[^1]
    * HR zones (static, configuring this is not yet implemented)
* Battery level synchronization
* Show notifications (has some bugs)
* Call notifications, with accept/reject (has some bugs on some devices, see [this for more information](../../internals/topics/huawei-honor-specifics.md#call-notifications))
* Music control
* ??? sublist "Activity synchronization"
    * Sleep synchronization (for TruSleep only begin/end time; no sleep stages)
    * Step count synchronization
    * Heart Rate synchronization (not supported by all devices)
    * SpO2 synchronization (not supported by all devices)
* ??? sublist "Workout synchronization"
    Which data is actually present depends on device and workout type.
    * HR for workout
    * speed
    * step rate
    * cadence
    * step length
    * ground contact time
    * impact
    * swing angle
    * fore foot landings
    * mid foot landings
    * back foot landings
    * eversion angle
    * swim style ([swimming](../../internals/topics/huawei-honor-specifics.md#swimming-workouts), per segment)
    * strokes ([swimming](../../internals/topics/huawei-honor-specifics.md#swimming-workouts), per segment)
    * distance ([swimming](../../internals/topics/huawei-honor-specifics.md#swimming-workouts), per segment)
    * swolf ([swimming](../../internals/topics/huawei-honor-specifics.md#swimming-workouts), per segment)
    * stroke rate ([swimming](../../internals/topics/huawei-honor-specifics.md#swimming-workouts), per segment)
    * calories
    * cycling power
    * altitude
    * GPS track
* Find my phone (must be paired as companion device)
* ??? sublist "Weather support (not supported by all devices)"
    * Current weather
    * Hourly forecast
    * Daily forecast
    * Sunrise/set
    * Moonrise/set
    * Phase of the moon
* Sending GPS from the phone to the band during workouts[^2]
* Uploading watch faces
* Remote camera shutter support (limited functionality)
* Uploading music over Bluetooth (not supported by all devices)
    * See [this for more information](../../internals/topics/huawei-honor-specifics.md#installing-music)
* Calendar sync support (not supported by all devices)
* Contact sync support (not supported by all devices)
* Basic support for installing applications (limited functionality, not supported by all devices)
    * See {{ 4175|issue }} for more details

[^1]: Only retrieved/synchronized on activity or workout synchronization.
[^2]: We had hoped this would enable workouts like "Outdoor Cycling", but that is not working yet.

See the [Specifics for Huawei and Honor devices](../../internals/topics/huawei-honor-specifics.md) for an overview of how the Huawei/Honor devices differ from other devices in Gadgetbridge.

## Known issues

!!! note "Open issues"
    Do not forget to check any other [open issues](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues?q=&type=all&sort=&labels=37069%2c-1238&state=open&milestone=0&project=0&assignee=0&poster=0) in the project repository.

See the [Specifics for Huawei and Honor devices](../../internals/topics/huawei-honor-specifics.md) for more details.

## Not supported (yet)

* Sleep stages for TruSleep
  * If your watch supports sleep without TruSleep enabled, it will show light/deep sleep when TruSleep is disabled. Not all watches support this.
* Tidal information support
* Stress measuring
* Starting a workout from GB
* Real time data
* Firmware operations (like updating)
* Band info screen selection (choice & order)
* Sending phone volume to the band (for the music control)
* Allow the user to set up their own HR zones
* Synchronizing skin temperature data

Note that this list is incomplete.


## Pair without factory reset

As of {{ 3721|pull }} it is possible to pair a device with Gadgetbridge without factory reset after using the official app. For this, you need to get the Huawei account ID, a 17-digit number.

### Through the website

#### On Firefox:

1. Go to https://cloud.huawei.com/ and log in
2. Press F12
3. Go to the `Storage` tab in the newly opened view
4. If not selected, select `https://cloud.huawei.com/` under the `Cookies` header on the left (this should be selected by default)
5. In the `Name` column, look for `userId`. In the `Value` column, you should see a 17 digit number.
6. Copy that key into your clipboard and jump to the next section to continue.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

#### On Chrome/a Chromium based browser:

1. Go to https://cloud.huawei.com/ and log in
2. Press F12
3. Go to the `Application` tab in the newly opened view
4. To the left under `Storage`, click the little downward arrow next to `Cookies` and open the cookie named `https://cloud.huawei.com/`
5. In the `Name` column, look for `userId`. In the `Value` column, you should see a 17 digit number.
6. Copy that key into your clipboard and jump to the next section to continue.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

### On rooted phones

Run following command in a root shell. (e.g. Termux for on-device, or ADB for on-computer)

```
grep old_user_id /data/data/com.huawei.health/shared_prefs/login_data.xml
```

This should output something similar to this:

```
 <string name="old_user_id">20007000023812345</string>
```

The account ID in this example would be `20007000023812345`.

Now, keep that key in your clipboard and jump to the next section to continue.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

### Without root

It has also been reported that the key is printed to logcat by the official app.

Use the `adb logcat` command and search for `huid=`, you should be able to find the value for the key.

[:material-arrow-down: Entering key](#entering-key){: .gb-button }

### Entering key

--8<--
pairing.md:auth_key_first
--8<--

![](../../assets/static/screenshots/auth_key.jpg){: height="600" style="height: 600px;" }

After entering your account ID, you should be able to connect to both the official app and Gadgetbridge without resetting the band every time.

Note that reconnecting to Gadgetbridge requires you to pair the device again - you can do this by disconnecting the device first, and then using the **:material-plus-box: Add** on the bottom-right corner of the home screen to pair the device again.
You do not delete the device before you do this!
Deleting the device will also delete the associated data!

Now you're ready to start using Gadgetbrigde!

[:material-arrow-right: Configuring your gadget](../features/index.md){: .gb-button }
