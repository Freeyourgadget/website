---
title: Garmin watches
---

# Garmin watches

The following list includes all implemented features for Garmin watches. The actual supported features can vary for each device.

!!! warning "Experimental"
    Support for these devices is recent, and still experimental. This page is also still a work in progress and not complete.

List of Garmin watches supported by Gadgetbridge:

<!--
    This is an auto-populated list from gadgets which has "os_garmin" flag,
    see "device_support.yml" file for more details.

    It basically creates a list item for every gadget and
    changes the casing for setting as display name for links.
    (like "garmin_forerunner_245" becomes to "Garmin Forerunner 245")

    Then, an anchor ID appends to the end of the page link,
    so when clicked on it, it will point to the related gadget's own section.
-->
{% for key, device in device_support.items() | sort %}
    {%- if "os_garmin" in device.flags %}
        {% set name = key.replace("_", " ").title() %}
        {% set icon = ' :material-flask-empty-outline:{: title="Experimental" }' if "experimental" in device.flags else "" %}
* [{{ name }}](../../gadgets/wearables/{{ device.vendor }}.md#device__{{ key }}) {{ icon }}
    {% endif -%}
{% endfor %}

### Implemented features

These features are supported by Gadgetbridge and apply to all Garmin watch models included in this page. Note that actual available features per device depends on the device capabilities.

??? note "List of features supported by Gadgetbridge"

    - Set time
    - Device info (firmware, model)
    - Device state (battery, sleep, wearing)
    - Calendar sync
    - Notifications, calls
    - Canned messages for calls and notifications
    - Contacts
    - Send GPS during workouts
    - AGPS upload
    - Activity sync
    - Realtime Settings
    - Weather

### Activity Sync

The following fit file types are fetched from the watch and saved in the phone's internal storage:

* Activity (workouts)
* Monitor (daily activity)
    * Steps
    * Heart rate
    * SpO2
    * Stress
    * Body energy (no UI - {{ 3943|issue }})
* Metrics (not parsed)
* Changelog (not parsed)
* HRV values and summary (nightly - {{ 3953|pull }})
* Sleep times and sleep stages on devices that provide them - see known issues below

### Pairing

Some watches like the Forerunner 165 need to be put in "pairing mode" for Gadgetbridge to be able to find them.

* In the watch, find a "Pair phone" option
* Your watch might now show a QR code - you can ignore it and simply pair on Gadgetbridge

Some devices might have issues pairing if not connected at least once to the official app - see {{ 3963|issue }}. So far, this has been reported to happen in the following devices:

* Fenix 5 Plus
* Fenix 5X Plus
* Forerunner 245 Music

### Known issues

!!! note "Open issues"
    Do not forget to check any other [open issues](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues?q=&type=all&sort=&labels=-1238%2c134442&state=open&milestone=0&project=0&assignee=0&poster=0) in the project repository.

* Activity sync sometimes gets stuck (eg. Fenix 7S)
* Sleep stages are only supported on devices that are able to display them without a phone connection - {{ 4048|issue }}

### Missing features

- User information
- Settings on older watches
- Alarms
- Hydration reminder
- App management

There are more missing features that are not listed here.

### Weather

Weather updates require authentication - see the [Authentication](#authentication) section.

### AGPS updates

Watches fetch AGPS updates periodically, by sending HTTP requests directly. Gadgetbridge intercepts these requests and sends a file from the local phone storage. These can be configured from the "Location" page in the device preferences.

AGPS updates require authentication on some devices - see the [Authentication](#authentication) section.

### Authentication

!!! examplke "Nightly only"
    This functionality is not yet released. It is already available in the [nightly releases.](../../index.md#nightly-releases)

Some functionalities such as weather and AGPS updates require the watch to be authenticated with valid credentials. These must be refreshed every 90 days for these functions to continue working.

Gadgetbridge supports sending fake credentials, so that the watch stays in an authenticated state - you can enable this in the "Authentication" settings page, by selecting "Send fake OAuth responses".

!!! warning "Official app"
    Do not enable if you plan on using the official app or connect the watch to the internet. After enabling this setting, your watch credentials will be invalid, and you may need to factory reset the watch to authenticate again.

## Import Historical Data from Garmin Connect

### 1. Export Your Garmin Data

To start, you need to export all your Garmin data from Garmin Connect.

1. Navigate to the [Garmin Account Data Management page](https://www.garmin.com/account/datamanagement/).
2. Sign in to your Garmin account.
    - If you are already signed in, skip this step
3. Select **Export Your Data**.
4. Click on **Request Data Export**.
    - Your request will be submitted, and you will receive an email with a download link. The link usually arrives within 48 hours, but it can take up to 30 days to be sent.
    - **Note:** For individual activity data to upload to a new account or third-party site, you can find it by navigating to `DI_CONNECT > DI-Connect-Fitness-Uploaded-Files` and opening the `.zip` files inside.

#### Alternative: Manual Steps from Garmin Support

- If you prefer not to visit the link, here are the steps directly from Garmin Support:
    1. Go to [Garmin Support](https://support.garmin.com/en-US/?faq=W1TvTPW8JZ6LfJSfK512Q8).
    2. Follow the steps listed under **"Export All Garmin Data Using Account Management Center"** to export your data.


### 2. Download Your Exported Data

Once you receive the export email from Garmin:
- Click the **Download** button in the email.
- Save the exported file to your preferred folder.


### 3. Unzip the Data Export

- Extract the contents of the exported `.zip` file.


### 4. Extract Files from the `.zip`

- Inside the unzipped folder, navigate to `DI_CONNECT > DI-Connect-Fitness-Uploaded-Files`.
- Extract any additional `.zip` files contained within this folder.


### 5. Connect Your Android Phone

You have two options for transferring the data to your Android phone: **MTP (Media Transfer Protocol)** or **ADB (Android Debug Bridge)**.

#### Option A: Using MTP (Recommended for smaller file counts)

1. Plug your Android phone into your computer using a USB cable.
2. Enable **File Transfer** on your Android phone.
    - This option will allow you to drag and drop files.

#### Option B: Using ADB (Recommended for larger file counts)

1. Enable **USB Debugging** in Developer Options.
   - [How to enable Developer Options](https://developer.android.com/studio/debug/dev-options)
2. Once USB Debugging is enabled, connect your phone to the computer.


### 6. Transfer the Data to Your Android Device

Depending on which method you are using, follow one of the options below.

#### Option A: Using MTP

- Navigate to the extracted `.zip` folder and drag the files into the following folder on your Android device:
  - **Path on Android:**
    ```
    /Android/data/nodomain.freeyourgadget.gadgetbridge.nightly/files/MAC:ADDRESS:HERE:XX:XX:XX/
    ```

#### Option B: Using ADB

- Open the terminal on your computer and run the following commands:
   1. Check if your device is recognized:
      ```bash
      adb devices
      ```
      - Ensure your device is listed.
   2. Push the files to your Android device:
      ```bash
      find ~/PATH/TO/DI_CONNECT/DI-Connect-Uploaded-Files/UploadedFiles_0-_Part1/ -type f -print0 | xargs -0 -I {} adb push {} /storage/emulated/0/Android/data/nodomain.freeyourgadget.gadgetbridge.nightly/files/MAC:ADDRESS:HERE:XX:XX:XX/
      ```


### 7. Open Gadgetbridge

- Once the files are successfully transferred, open the **Gadgetbridge** app on your Android phone.


### 8. Access Debug Menu

1. Tap the **Menu** hamburger (three horizontal lines) at the top-left of the **Dashboard**.
2. Tap on **Debug**.


### 9. Test New Functionality

1. Tap the button labeled **Test New Functionality**.
2. Once the **Scan Files** notification completes, your historical Garmin Connect data should now appear in Gadgetbridge.


### Additional Notes:

- If you're having issues with ADB or the MTP transfer method, make sure USB Debugging is enabled and that your device is correctly connected.
- If the data doesn't appear immediately in Gadgetbridge, try restarting the app or your phone.
