---
title: Navigation apps
---

# Navigation apps

![](../../assets/static/screenshots/graphic/integration_navigation.png)

Gadgetbridge supports multiple navigation instructions providers that can be enabled separately in the general settings.

## OsmAnd(+)

[OsmAnd](https://osmand.net/){: target="_blank" } is an open source navigation app utilizing [OpenStreetMap](https://welcome.openstreetmap.org/){: target="_blank" } data. Gadgetbridge autodetects which version is installed and connects to the app's API in the background. If multiple versions of OsmAnd are installed, it is possible to manually choose a version in the Gadgetbridge settings. Note that due to the implementation in Gadgetbridge it is necessary to disconnect and reconnect your gadget before a change to this setting takes effect.

The open source version of OsmAnd can be installed from [:simple-fdroid: F-Droid](https://f-droid.org/en/packages/net.osmand.plus/){: target="_blank" }.

## Google Maps

[:simple-googleplay: Google Maps](https://play.google.com/store/apps/details?id=com.google.android.apps.maps){: target="_blank" } is a proprietary navigation app, but regarded by many as one of the best. Gadgetbridge supports forwarding this app's navigation instructions by parsing its persistent notification. Due to this method, it is required to allow the Maps notifications in the Gadgetbridge notification settings for this integration to work.
