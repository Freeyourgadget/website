---
title: Integrations
icon: material/puzzle
---

# Integrations

This category contains information about how Gadgetbridge and external apps can be combined for a more complete experience.

{{ file_listing() }}
