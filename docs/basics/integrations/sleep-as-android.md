---
title: Sleep as Android
---

# Sleep as Android

![](../../assets/static/screenshots/graphic/integration_activity.png)

Gadgetbridge can provide data from paired gadgets to proprietary [:simple-googleplay: Sleep as Android](https://play.google.com/store/apps/details?id=com.urbandroid.sleep) app, allowing Sleep as Android to collect & track sleep statistics with your gadgets paired in Gadgetbridge. Support for Sleep as Android has been added in {{ 3687|pull }}.

## Supported devices

- [Zepp OS](../topics/zeppos.md) devices
- Bangle.js (with {{ 3785|pull }}), requires [installing the provider](#banglejs) see below
- Pebble devices support a standalone separate integration - see the [PebbleKit](../topics/pebblekit.md#sleep-as-android)

## Setting up

After installing Sleep as Android, enable the Wearables add-on, with selecting the "DIY" option since Sleep as Android has not yet whitelisted Gadgetbridge.

Then, use the package name matching the version of Gadgetbridge you have installed:

| Version | Package |
|:--------|:--------|
| Mainline | `nodomain.freeyourgadget.gadgetbridge` |
| Bangle.js | `com.espruino.gadgetbridge.banglejs` |
| Nightly | `nodomain.freeyourgadget.gadgetbridge.nightly` |
| Nightly Bangle.js | `com.espruino.gadgetbridge.banglejs.nightly` |
| Nightly No Pebble | `nodomain.freeyourgadget.gadgetbridge.nightly_nopebble` |

In Gadgetbridge, you must enable the Sleep as Android integration in the preferences, as well as select the device that will be used to send data to Sleep as Android.

### Bangle.js

Along with above steps, additionally, [Acceleration Data Provider](https://banglejs.com/apps/?id=accelsender) app must be installed on Bangle.js to send acceleration data over Bluetooth, thus can be used for Sleep as Android.
