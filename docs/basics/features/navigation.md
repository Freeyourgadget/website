---
title: Navigation
---

# Navigation

Some gadgets support displaying navigation instructions. Gadgetbridge can receive those instructions from several apps and forward them to the gadget.

<div class="gb-figure-container" markdown="span">
    <div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/demo/demo_fossil_navigation.png){: height="240" style="height: 240px;" }
        Navigation instruction on Fossil watch
    </div>
</div>

## Supported devices

Navigation instructions are currently supported by the following devices:

- Bangle.js
- Fossil Hybrid HR (needs unofficial app)
- PineTime

## Setting up

Continue reading on [Navigation apps](../integrations/navigation.md) for a list of supported navigation apps and instructions on how to set them up.
