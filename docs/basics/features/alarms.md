---
title: Alarms
---

# Alarms

Some gadgets have and alarms feature. If your gadget supports alarms and Gadgetbridge has support of syncing alarms to your gadget, you can configure alarms in Gadgetbridge. Tap on the **:material-alarm: Alarm** icon on the home screen to manage alarms.

![](../../assets/static/screenshots/device_alarms.jpg){: height="600" style="height: 600px;" }

## Setting up

Tap on an alarm to set its time. Then tap on the **:material-toggle-switch: Toggle** next to an alarm time to enable or disable it.

If your gadget has a small screen, sending all alarms to the gadget can be a problem because the screen size may not be good for scrolling through so many alarms. This is why Gadgetbridge allows you to exclude alarms to prevent syncing them with the gadget.

If an alarm is grayed out, this means that alarm won't be sent to the gadget. So you won't see the alarm on your gadget, and because it is never sent to gadget, the alarm won't ring regardless of its toggle. To prevent an alarm sending to your gadget, long tap on the alarm to make it grayed out (do the same to undo).

So in short:

* Grayed out and toggle is off: won't be saved to gadget, the alarm won't ring.
* Grayed out and toggle is on: won't be saved to gadget, the alarm won't ring.
* Toggle is on: will be saved to gadget, alarm **WILL** ring.
* Toggle is off: will be saved to gadget, alarm won't ring.

Alarms configured in Gadgetbridge are sent to the gadget during Bluetooth connect and when you exit from the alarms screen.
