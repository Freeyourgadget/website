---
title: Find phone
---

# Find phone

Gadgetbridge opens a window on the screen and plays a sound when the "Find phone" feature on the gadget has activated.

## Setting up

In most cases, you don't need to do anything to enable it. However, on some custom ROMs such as MIUI, it may be necessary to manually grant some permissions:

* Show on lockscreen
* Open new windows on background

See {{ 3309|issue }} for more details.

Also, in order for the "Find phone" feature to work correctly on Android 10 and up, Android requires that the pairing between the phone and the gadget is done via "Companion device", see [Companion device pairing](../pairing/companion-device.md) for more details.

### Adjusting the ring tone

By default, the Incoming call ringing tone will be used when the Find phone feature is used but you can customize this via "Gadgetbridge menu → Notification settings → Ping tone".
