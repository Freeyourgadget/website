---
title: Synchronize data
---

# Synchronize data

To synchronize activity data from the gadget to Gadgetbridge, press the **:material-refresh: Sync** icon under your gadget name on the home screen. By default, Gadgetbridge will not synchronize activity data unless requested manually.

Some gadgets send this data to your phone automatically (like Pebble) so the sync icon is not present for these gadgets.

!!! warning "Warning"
    In order to receive data, make sure that no other app is syncing and removing the data from your gadget. Therefore make sure that the official app is not running. Kill the app (this might be not possible as every notification might start it up again) or uninstall it.

<div class="gb-figure-container" markdown="span">
    <div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/home_sync.png){: height="600" style="height: 600px;" }
    </div>
</div>

## Auto-fetch

If you don't want to press **:material-refresh: Sync** icon on each time you want to synchronize the activity data, you may want Gadgetbridge to automatically pull activity data every time the screen is unlocked. It will only work if there is a screen lock set.

<div class="gb-step" markdown>
![](../../assets/static/screenshots/settings_auto_fetch.png){: height="600" style="height: 600px;" }
<div markdown>

To do so, enable "**:material-cog: Settings** → Auto fetch activity data".

Below the auto fetch option, you can also set a minimum duration (in minutes) between synchronizations.

To access the settings, while on the home screen, open sidebar (tap the **:material-menu: Menu** icon or swipe from the left side to the right) and tap **:material-cog: Settings**.

</div></div>

## For developers

Gadgetbridge can also perform a synchronization programmatically with [Synchronize Data API.](../../internals/automations/intents.md#synchronize-data-api)
