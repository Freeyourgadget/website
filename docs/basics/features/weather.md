---
title: Weather
---

# Weather

If you configure it properly and your gadget supports it, Gadgetbridge will receive weather and temperature information from a weather provider for your location, then send it to your gadget.

!!! abstract "Additional setup needed"
    To ensure that none of the accessed data is ever forwarded anywhere, Gadgetbridge does not have (and will not have) the network permission. So, Gadgetbridge requires a "Weather provider" application which is installed on your Android device that can retrieve the weather information and provide it to Gadgetbridge.

<div class="gb-figure-container" markdown="span">
    <div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/demo/demo_mi_band_weather.png){: height="300" style="height: 300px;" }
        Weather on gadget
    </div>
</div>

## Setting up

Continue reading on [Weather providers](../integrations/weather.md) for information about weather apps that communicate with Gadgetbridge and how to set them up.

## Additional settings

You can change "Settings → Language and region settings → Units" in Gadgetbridge which is used for weather on some gadgets (Huami, Pebble and some others).

Some gadgets can also show sunrise/sunset information, for this you can set your latitude and longitude in **:material-menu: Menu** → **:material-cog: Settings** → Location.
