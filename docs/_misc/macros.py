from typing import TYPE_CHECKING, Dict, Any, Optional, Union
from importlib.util import find_spec
from functools import lru_cache
from pathlib import Path

if TYPE_CHECKING:
    from mkdocs_macros.plugin import MacrosPlugin

def get_icon_finder():
    spec = find_spec("material")
    if (not spec) or (not spec.origin):
        raise ValueError("Couldn't find 'material' module.")
    icons_path = Path(spec.origin).parent / "templates/.icons"
    def get_icon(name : str) -> str:
        icon = icons_path / (name + ".svg")
        with icon.open("r", encoding = "utf-8") as f:
            return f.read()
    return lru_cache(maxsize = None)(get_icon)

# https://mkdocs-macros-plugin.readthedocs.io/en/latest/
def define_env(env : "MacrosPlugin"):
    widget_config : Dict[str, Dict[str, Any]] = env.variables.get("gb_widgets", {})
    git = widget_config.get("git_links", {}).get("config", {})
    repo_url = env.conf.get("repo_url", "")
    icon_loader = get_icon_finder()

    @env.filter
    def issue(number : int, comment : Optional[int] = None):
        if (not widget_config) or (not widget_config.get("git_links", {}).get("enabled", True)):
            return \
                "issue #" + str(number) + \
                ("" if not comment else "#" + str(comment))
        return '[{name}]({link}){{ title="{title}" target="_blank" }}'.format(
            name = git["issue_comment" if comment else "issue"].format(number),
            link = git["issue_comment_url" if comment else "issue_url"].format(
                number = number,
                comment = comment,
                repo_url = repo_url
            ),
            title = git["issue_hint"].format(number)
        )

    @env.filter
    def pull(number : int, comment : Optional[int] = None):
        if (not widget_config) or (not widget_config.get("git_links", {}).get("enabled", True)):
            return \
                "pull request #" + str(number) + \
                ("" if not comment else "#" + str(comment))
        return '[{name}]({link}){{ title="{title}" target="_blank" }}'.format(
            name = git["pull_comment" if comment else "pull"].format(number),
            link = git["pull_comment_url" if comment else "pull_url"].format(
                number = number,
                comment = comment,
                repo_url = repo_url
            ),
            title = git["pull_hint"].format(number)
        )

    @env.filter
    def commit(commit_sha : str, comment : Optional[str] = None):
        if (not widget_config) or (not widget_config.get("git_links", {}).get("enabled", True)):
            return \
                "commit #" + str(commit_sha) + \
                ("" if not comment else "#" + str(comment))
        return '[{name}]({link}){{ title="{title}" target="_blank" }}'.format(
            name = git["commit"].format(commit_sha[:10]),
            link = git["commit_url"].format(
                commit_sha = commit_sha,
                comment = comment,
                repo_url = repo_url
            ),
            title = git["commit_hint"].format(commit_sha[:10])
        )

    @env.filter
    def color(color : str):
        color_config = widget_config.get("color", {})
        # If widget has disabled, only render it as a inline code block.
        if (not color_config) or (not color_config.get("enabled", True)):
            return f"`{color.upper()}`"
        return (
            '<span data-clipboard-text="{color}">'
            '<span class="{class_name}" style="background-color: {color};"></span>'
            '`{color}`</span>'
        ).format(
            class_name = color_config["class_name"],
            color = color if not color.startswith("#") else color.upper()
        )

    @env.macro
    def file_listing():
        result = []
        assert env.page.parent
        for child in env.page.parent.children:
            filename = child.file.src_uri.split('/')[-1] # type: ignore
            title = filename[:-3].replace("-", " ").title()
            # Exclude current page.
            if child == env.page:
                continue
            result.append(f"* [{title}]({filename})")
        return \
            "Pages under this section:\n\n" + "\n".join(result)

    def render_tag(
        class_name : str,
        description : str,
        icon : str,
        name : str,
        extra : Union[str, bool] = False,
        link : Optional[str] = None,
        copy_text : Optional[str] = None
    ):
        tag = '<span class="{class_name}" title="{title}"{attr}>:{icon}: {name}{extra}</span>'.format( # noqa: E501
            class_name = class_name,
            title = description,
            icon = "" if not icon else icon.replace("/", "-", 1),
            name = name,
            extra = extra if isinstance(extra, str) else \
                ' :material-launch:{: style="margin: 0 0 0 10px !important;" }' \
                if ((extra is True) and link) else "",
            attr = "" if not copy_text else f' data-clipboard-text="{copy_text}"'
        )
        if link:
            return '<a href="{href}">{tag}</a>'.format(href = link, tag = tag)
        return tag

    @env.macro
    def device(device_id : str):
        """
        Creates device support labels, for a device code.
        See "device_support.yml" file for details.
        """
        config = widget_config.get("label", {})
        output = ""
        # Create anchor link for the header.
        header_id = "device__" + device_id
        output += "{{: #{0} }}".format(header_id)
        # If this feature has been disabled by configuration,
        # don't create labels.
        if (not config) or (not config.get("enabled", True)):
            return output
        output += '\n\n'
        # Get device metadata
        device_data = env.variables["device_support"][device_id]
        # Display support level in different style,
        # so we pick for the "feature" flag of the device.
        support_flag = next((x for x in device_data["flags"] if x.startswith("feature_")), None) # noqa: E501
        if not support_flag:
            raise ValueError(
                f"Device '{device_id}' doesn't have a support level flag set. " + \
                "If it is unknown, add 'feature_unknown' flag explictly."
            ) # noqa: E501
        facts = []
        # We add the support flag itself manually to the first item,
        # so it will be shown first on the website.
        facts.append({
            "id": support_flag,
            "type": "support",
            "name": config["values"][support_flag]["name"],
            "description": config["values"][support_flag]["description"],
            "color": config["values"][support_flag].get("color", None),
            "icon": config["values"][support_flag]["icon"]
        })
        flags_list = []
        for flag in device_data["flags"]:
            label = config["values"][flag]
            if label.get("hidden", False):
                continue
            # Ignore the feature flags, we already added them manually,
            # but it might make sense for checking for duplicates 
            # in the future.
            if flag.startswith("feature_"):
                continue
            flags_list.append({
                "id": flag,
                "type": "flag",
                "name": label["name"],
                "description": label["description"],
                "color": label.get("color", None),
                "icon": label["icon"],
                "link": label.get("link", None)
            })
        # Put pairing information to the first when sorting, 
        # as it is more important to know.
        flags_list = sorted(flags_list, key = lambda x: "" if x["id"].startswith("pair_") else x["id"]) # noqa: E501
        facts.extend(flags_list)
        links_list = []
        for link_type, link_source in device_data.get("links", {}).items():
            label = config["links"][link_type]
            links_list.append({
                "id": link_type,
                "type": "link",
                "name": label["name"],
                "description": label["description"],
                "color": None,
                "icon": label["icon"],
                "link": link_source
            })
        links_list = sorted(links_list, key = lambda x: x["id"])
        facts.extend(links_list)
        output += '<div class="gb-device">'
        output += f'<a class="gb-device-summary" tabindex="0" href="#{header_id}">' # noqa: E501
        for fact in facts:
            # Don't display any icons for links while support
            # labels are not expanded.
            if fact["type"] == "link":
                continue
            output += '<span class="gb-device-tag{class_name}">{icon}{name}</span>'.format( # noqa: E501
                class_name = "" if not fact["color"] else f' gb-color-{fact["color"]}',
                # Only display an icon for other flags
                name = "" if fact["type"] != "support" else fact["name"],
                icon = icon_loader(fact["icon"])
            )
        output += '</a><div class="gb-device-info">'
        is_section_displayed = False
        for fact in facts:
            if (fact["type"] == "link") and (not is_section_displayed):
                output += '<span class="gb-device-info-title">Extras</span>'
                is_section_displayed = True
            tag_name = "a" if fact.get("link", None) else "div"
            attr = "" if tag_name == "div" else f'href="{fact["link"]}"'
            class_name = "" if not fact["color"] else f' gb-color-{fact["color"]}'
            output += f'<{tag_name} class="gb-device-info-fact{class_name}" {attr}>' # noqa: E501
            output += f'<span class="gb-device-info-icon">{icon_loader(fact["icon"])}</span>' # noqa: E501
            output += '<div class="gb-device-info-text">'
            output += f'<span class="gb-device-info-text-line">{fact["name"]}</span>'
            output += f'<span class="gb-device-info-text-line">{fact["description"]}</span>' # noqa: E501
            output += f'</div></{tag_name}>'
        output += '</div></div>'
        return output

    @env.macro
    def get_gadget_breakdown():
        stats = [0, 0, 0, 0, 0, []]
        for _, i in env.variables["device_support"].items():
            stats[0] += 1
            if ("feature_high" in i["flags"]) or ("feature_most" in i["flags"]):
                stats[1] += 1
            elif "feature_partial" in i["flags"]:
                stats[2] += 1
            elif "feature_poor" in i["flags"]:
                stats[3] += 1
            else:
                stats[4] += 1
            if i["vendor"] not in stats[5]:
                stats[5].append(i["vendor"])
        return {
            "all": str(stats[0]), # It needs to be str as it will be splitted to digits.
            "good": stats[1],
            "meh": stats[2],
            "poor": stats[3],
            "unknown": stats[4],
            "vendor": len(stats[5])
        }